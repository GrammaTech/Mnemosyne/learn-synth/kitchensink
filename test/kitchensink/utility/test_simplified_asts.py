"""
Unit tests for the AST simplification module.
"""

from pathlib import Path
from typing import Dict
from asts import AST, ASTLanguage
from kitchensink.utility.filesystem import slurp
from kitchensink.utility.simplified_asts import SimplifiedAST

DATA_DIR = Path(__file__).parent / "data" / "simplified_asts"


def test_simplify_ast():
    def parse_and_simplify(path: Path) -> Dict:
        text = slurp(path)
        root = AST.from_string(text, language=ASTLanguage.Python)
        function = root.children[-1]
        return SimplifiedAST.from_sel(root, function).to_json()

    expected = [
        "##",
        [
            "if#:#",
            {"leading": "\n    ", "token": "if", "trailing": ""},
            {"leading": " ", "leaf": True, "token": "a", "trailing": ""},
            {"leading": "", "token": ":", "trailing": ""},
            [
                "#",
                [
                    "#",
                    [
                        "return#",
                        {"leading": "\n        ", "token": "return", "trailing": ""},
                        {
                            "leading": " ",
                            "leaf": True,
                            "token": "a",
                            "trailing": "",
                        },
                    ],
                ],
            ],
        ],
        [
            "#",
            [
                "return#",
                {"leading": "\n    ", "token": "return", "trailing": ""},
                {"leading": " ", "leaf": True, "token": "0", "trailing": ""},
            ],
        ],
    ]
    assert parse_and_simplify(DATA_DIR / "if1.py") == expected

    expected = [
        "#",
        [
            "for#in#:#",
            {"leading": "\n    ", "token": "for", "trailing": ""},
            {"leading": " ", "leaf": True, "token": "i", "trailing": ""},
            {"leading": " ", "token": "in", "trailing": ""},
            {"leading": " ", "leaf": True, "token": "a", "trailing": ""},
            {"leading": "", "token": ":", "trailing": ""},
            [
                "#",
                [
                    "#",
                    [
                        "##",
                        {
                            "leading": "\n        ",
                            "leaf": True,
                            "token": "print",
                            "trailing": "",
                        },
                        [
                            "(#)",
                            {"leading": "", "token": "(", "trailing": ""},
                            {
                                "leading": "",
                                "leaf": True,
                                "token": "i",
                                "trailing": "",
                            },
                            {"leading": "", "token": ")", "trailing": ""},
                        ],
                    ],
                ],
            ],
        ],
    ]
    assert parse_and_simplify(DATA_DIR / "for1.py") == expected

    expected = [
        "##",
        [
            "#",
            [
                "#=#",
                {
                    "leading": "\n    ",
                    "leaf": True,
                    "token": "i",
                    "trailing": "",
                },
                {"leading": " ", "token": "=", "trailing": ""},
                {"leading": " ", "leaf": True, "token": "0", "trailing": ""},
            ],
        ],
        [
            "while#:#",
            {"leading": "\n    ", "token": "while", "trailing": ""},
            [
                "#<#",
                {
                    "leading": " ",
                    "leaf": True,
                    "token": "i",
                    "trailing": "",
                },
                {"leading": " ", "token": "<", "trailing": ""},
                [
                    "##",
                    {"leading": " ", "leaf": True, "token": "len", "trailing": ""},
                    [
                        "(#)",
                        {"leading": "", "token": "(", "trailing": ""},
                        {
                            "leading": "",
                            "leaf": True,
                            "token": "a",
                            "trailing": "",
                        },
                        {"leading": "", "token": ")", "trailing": ""},
                    ],
                ],
            ],
            {"leading": "", "token": ":", "trailing": ""},
            [
                "#",
                [
                    "#",
                    [
                        "##",
                        {
                            "leading": "\n        ",
                            "leaf": True,
                            "token": "print",
                            "trailing": "",
                        },
                        [
                            "(#)",
                            {"leading": "", "token": "(", "trailing": ""},
                            [
                                "##",
                                {
                                    "leading": "",
                                    "leaf": True,
                                    "token": "a",
                                    "trailing": "",
                                },
                                [
                                    "[#]",
                                    {"leading": "", "token": "[", "trailing": ""},
                                    {
                                        "leading": "",
                                        "leaf": True,
                                        "token": "i",
                                        "trailing": "",
                                    },
                                    {"leading": "", "token": "]", "trailing": ""},
                                ],
                            ],
                            {"leading": "", "token": ")", "trailing": ""},
                        ],
                    ],
                ],
            ],
        ],
    ]
    assert parse_and_simplify(DATA_DIR / "while1.py") == expected

    expected = [
        "#",
        [
            "try:###",
            {"leading": "\n    ", "token": "try", "trailing": ""},
            {"leading": "", "token": ":", "trailing": ""},
            [
                "#",
                [
                    "#",
                    [
                        "#=#",
                        {
                            "leading": "\n        ",
                            "leaf": True,
                            "token": "a",
                            "trailing": "",
                        },
                        {"leading": " ", "token": "=", "trailing": ""},
                        [
                            "#/#",
                            {
                                "leading": " ",
                                "leaf": True,
                                "token": "1",
                                "trailing": "",
                            },
                            {"leading": " ", "token": "/", "trailing": ""},
                            {
                                "leading": " ",
                                "leaf": True,
                                "token": "a",
                                "trailing": "",
                            },
                        ],
                    ],
                ],
            ],
            [
                "except#as#:",
                {"leading": "\n    ", "token": "except", "trailing": ""},
                {
                    "leading": " ",
                    "leaf": True,
                    "token": "ZeroDivisionError",
                    "trailing": "",
                },
                {"leading": " ", "token": "as", "trailing": ""},
                {
                    "leading": " ",
                    "leaf": True,
                    "token": "e",
                    "trailing": "",
                },
                {"leading": "", "token": ":", "trailing": ""},
            ],
            [
                "#",
                [
                    "#",
                    [
                        "##",
                        {
                            "leading": "\n        ",
                            "leaf": True,
                            "token": "print",
                            "trailing": "",
                        },
                        [
                            "(#)",
                            {"leading": "", "token": "(", "trailing": ""},
                            {
                                "leading": "",
                                "leaf": True,
                                "token": "e",
                                "trailing": "",
                            },
                            {"leading": "", "token": ")", "trailing": ""},
                        ],
                    ],
                ],
            ],
        ],
    ]
    assert parse_and_simplify(DATA_DIR / "try1.py") == expected

    expected = [
        "##",
        [
            "#",
            [
                "#=#",
                {
                    "leading": "\n    ",
                    "leaf": True,
                    "token": "s",
                    "trailing": "",
                },
                {"leading": " ", "token": "=", "trailing": ""},
                [
                    "#+#",
                    {
                        "leading": " ",
                        "leaf": True,
                        "token": "a",
                        "trailing": "",
                    },
                    {"leading": " ", "token": "+", "trailing": ""},
                    {"leading": " ", "leaf": True, "token": '"反复请求多次"', "trailing": ""},
                ],
            ],
        ],
        [
            "#",
            [
                "##",
                {
                    "leading": "\n" "    ",
                    "leaf": True,
                    "token": "print",
                    "trailing": "",
                },
                [
                    "(#)",
                    {"leading": "", "token": "(", "trailing": ""},
                    {
                        "leading": "",
                        "leaf": True,
                        "token": "s",
                        "trailing": "",
                    },
                    {"leading": "", "token": ")", "trailing": ""},
                ],
            ],
        ],
    ]
    assert parse_and_simplify(DATA_DIR / "utf8-1.py") == expected

    expected = [
        "#",
        [
            "#",
            [
                "return#",
                {"leading": "\n    ", "token": "return", "trailing": ""},
                [
                    "####",
                    {"leading": " ", "leaf": True, "token": "os", "trailing": ""},
                    [
                        ".#",
                        {"leading": "", "token": ".", "trailing": ""},
                        {"leading": "", "leaf": True, "token": "path", "trailing": ""},
                    ],
                    [
                        ".#",
                        {"leading": "", "token": ".", "trailing": ""},
                        {"leading": "", "leaf": True, "token": "join", "trailing": ""},
                    ],
                    [
                        "(#)",
                        {"leading": "", "token": "(", "trailing": ""},
                        [
                            "#,#",
                            {
                                "leading": "",
                                "leaf": True,
                                "token": "a",
                                "trailing": "",
                            },
                            {"leading": "", "token": ",", "trailing": ""},
                            {
                                "leading": " ",
                                "leaf": True,
                                "token": '"foo"',
                                "trailing": "",
                            },
                        ],
                        {"leading": "", "token": ")", "trailing": ""},
                    ],
                ],
            ],
        ],
    ]
    assert parse_and_simplify(DATA_DIR / "join1.py") == expected
    assert parse_and_simplify(DATA_DIR / "join2.py") == expected
    assert parse_and_simplify(DATA_DIR / "join3.py") == expected
    assert parse_and_simplify(DATA_DIR / "join4.py") == expected
    assert parse_and_simplify(DATA_DIR / "join5.py") == expected

    expected = [
        "###",
        [
            "try:###",
            {"leading": "\n    ", "token": "try", "trailing": ""},
            {"leading": "", "token": ":", "trailing": ""},
            [
                "##",
                [
                    "for#in#:#",
                    {"leading": "\n        ", "token": "for", "trailing": ""},
                    {
                        "leading": " ",
                        "leaf": True,
                        "token": "i",
                        "trailing": "",
                    },
                    {"leading": " ", "token": "in", "trailing": ""},
                    {
                        "leading": " ",
                        "leaf": True,
                        "token": "a",
                        "trailing": "",
                    },
                    {"leading": "", "token": ":", "trailing": ""},
                    [
                        "#",
                        [
                            "#",
                            [
                                "##",
                                {
                                    "leading": "\n            ",
                                    "leaf": True,
                                    "token": "print",
                                    "trailing": "",
                                },
                                [
                                    "(#)",
                                    {"leading": "", "token": "(", "trailing": ""},
                                    {
                                        "leading": "",
                                        "leaf": True,
                                        "token": "i",
                                        "trailing": "",
                                    },
                                    {"leading": "", "token": ")", "trailing": ""},
                                ],
                            ],
                        ],
                    ],
                ],
                [
                    "while#:#",
                    {"leading": "\n        ", "token": "while", "trailing": ""},
                    [
                        "#<#",
                        {
                            "leading": " ",
                            "leaf": True,
                            "token": "i",
                            "trailing": "",
                        },
                        {"leading": " ", "token": "<", "trailing": ""},
                        [
                            "##",
                            {
                                "leading": " ",
                                "leaf": True,
                                "token": "len",
                                "trailing": "",
                            },
                            [
                                "(#)",
                                {"leading": "", "token": "(", "trailing": ""},
                                {
                                    "leading": "",
                                    "leaf": True,
                                    "token": "a",
                                    "trailing": "",
                                },
                                {"leading": "", "token": ")", "trailing": ""},
                            ],
                        ],
                    ],
                    {"leading": "", "token": ":", "trailing": ""},
                    [
                        "#",
                        [
                            "#",
                            [
                                "##",
                                {
                                    "leading": "\n            ",
                                    "leaf": True,
                                    "token": "print",
                                    "trailing": "",
                                },
                                [
                                    "(#)",
                                    {"leading": "", "token": "(", "trailing": ""},
                                    [
                                        "#/#",
                                        [
                                            "##",
                                            {
                                                "leading": "",
                                                "leaf": True,
                                                "token": "a",
                                                "trailing": "",
                                            },
                                            [
                                                "[#]",
                                                {
                                                    "leading": "",
                                                    "token": "[",
                                                    "trailing": "",
                                                },
                                                {
                                                    "leading": "",
                                                    "leaf": True,
                                                    "token": "0",
                                                    "trailing": "",
                                                },
                                                {
                                                    "leading": "",
                                                    "token": "]",
                                                    "trailing": "",
                                                },
                                            ],
                                        ],
                                        {"leading": " ", "token": "/", "trailing": ""},
                                        [
                                            "##",
                                            {
                                                "leading": " ",
                                                "leaf": True,
                                                "token": "a",
                                                "trailing": "",
                                            },
                                            [
                                                "[#]",
                                                {
                                                    "leading": "",
                                                    "token": "[",
                                                    "trailing": "",
                                                },
                                                {
                                                    "leading": "",
                                                    "leaf": True,
                                                    "token": "i",
                                                    "trailing": "",
                                                },
                                                {
                                                    "leading": "",
                                                    "token": "]",
                                                    "trailing": "",
                                                },
                                            ],
                                        ],
                                    ],
                                    {"leading": "", "token": ")", "trailing": ""},
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            [
                "except#as#:",
                {"leading": "\n    ", "token": "except", "trailing": ""},
                {
                    "leading": " ",
                    "leaf": True,
                    "token": "ZeroDivisionError",
                    "trailing": "",
                },
                {"leading": " ", "token": "as", "trailing": ""},
                {
                    "leading": " ",
                    "leaf": True,
                    "token": "e",
                    "trailing": "",
                },
                {"leading": "", "token": ":", "trailing": ""},
            ],
            [
                "#",
                [
                    "#",
                    [
                        "##",
                        {
                            "leading": "\n        ",
                            "leaf": True,
                            "token": "print",
                            "trailing": "",
                        },
                        [
                            "(#)",
                            {"leading": "", "token": "(", "trailing": ""},
                            {
                                "leading": "",
                                "leaf": True,
                                "token": "e",
                                "trailing": "",
                            },
                            {"leading": "", "token": ")", "trailing": ""},
                        ],
                    ],
                ],
            ],
        ],
        [
            "if#:#",
            {"leading": "\n\n    ", "token": "if", "trailing": ""},
            {"leading": " ", "leaf": True, "token": "a", "trailing": ""},
            {"leading": "", "token": ":", "trailing": ""},
            [
                "#",
                [
                    "#",
                    [
                        "return#",
                        {"leading": "\n        ", "token": "return", "trailing": ""},
                        {
                            "leading": " ",
                            "leaf": True,
                            "token": "a",
                            "trailing": "",
                        },
                    ],
                ],
            ],
        ],
        [
            "#",
            [
                "return#",
                {"leading": "\n    ", "token": "return", "trailing": ""},
                {"leading": " ", "leaf": True, "token": "0", "trailing": ""},
            ],
        ],
    ]
    assert parse_and_simplify(DATA_DIR / "complex.py") == expected
