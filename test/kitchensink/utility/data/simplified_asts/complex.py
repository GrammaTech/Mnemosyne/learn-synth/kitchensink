def foo(a):
    try:
        for i in a:
            print(i)
        while i < len(a):
            print(a[0] / a[i])
    except ZeroDivisionError as e:
        print(e)

    if a:
        return a
    return 0
