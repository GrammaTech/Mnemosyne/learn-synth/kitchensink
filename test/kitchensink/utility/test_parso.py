"""
Unit tests for the AST utility module.
"""

import parso

from pathlib import Path
from typing import Callable, List, Text

from jedi import Script
from parso.python.tree import Module, BaseNode, PythonNode
from kitchensink.utility.filesystem import slurp
from kitchensink.utility.parso import (
    _import_aliases,
    _import_names,
    filter_asts,
    is_function,
    is_function_callsite,
    is_incomplete_function_callsite,
    is_keyword_argument,
    is_incomplete_keyword_argument,
    in_scope_imports,
    in_scope_import_froms,
    in_scope_names,
    callsite_signature,
    callsite_function,
    callsite_module,
    callargs,
    normalize_callarg,
    get_keyword,
)


DATA_DIR = Path(__file__).parent / "data" / "ast"


def positive_test(text: Text, predicate: Callable) -> None:
    """
    Assert that @text satisfies the given @predicate.

    :param text: source text
    :param predicate: boolean predicate taking a source text parameter
    """
    root = parso.parse(text)
    node = root.children[0]
    assert predicate(node)


def negative_test(text: Text, predicate: Callable) -> None:
    """
    Assert that @text does not satisfy the given @predicate

    :param text: source text
    :param predicate: boolean predicate taking a source text parameter
    """
    root = parso.parse(text)
    node = root.children[0]
    assert not predicate(node)


def test_is_function():
    positive_test(slurp(DATA_DIR / "is_function1.py"), is_function)
    positive_test(slurp(DATA_DIR / "is_function2.py"), is_function)

    negative_test("bar.foo()", is_function)
    negative_test("lambda a: a ** 2", is_function)
    negative_test("a = b", is_function)


def test_is_function_callsite():
    positive_test("foo()", is_function_callsite)
    positive_test("bar.foo()", is_function_callsite)
    positive_test("baz.bar.foo()", is_function_callsite)
    positive_test("baz.bar.foo(a, b, c)", is_function_callsite)

    negative_test("foo(", is_function_callsite)
    negative_test("bar.foo(a, b, c", is_function_callsite)
    negative_test("a = b", is_function_callsite)


def test_is_incomplete_function_callsite():
    positive_test("foo(", is_incomplete_function_callsite)
    positive_test("bar.foo(", is_incomplete_function_callsite)
    positive_test("baz.bar.foo(", is_incomplete_function_callsite)
    positive_test("baz.bar.foo(a, b,", is_incomplete_function_callsite)
    positive_test("baz.bar.foo(a, b, c", is_incomplete_function_callsite)
    positive_test("baz.bar.foo(a, b, f(", is_incomplete_function_callsite)
    positive_test("with baz.bar.foo(a, b, c", is_incomplete_function_callsite)
    positive_test("x = baz.bar.foo(a, b, c", is_incomplete_function_callsite)
    positive_test("x = baz.bar.foo(a, b, f(", is_incomplete_function_callsite)

    negative_test("foo()", is_incomplete_function_callsite)
    negative_test("bar.foo()", is_incomplete_function_callsite)
    negative_test("baz.bar.foo()", is_incomplete_function_callsite)
    negative_test("baz.bar.foo(a, b, c)", is_incomplete_function_callsite)
    negative_test("baz.bar.foo(a, b, f())", is_incomplete_function_callsite)
    negative_test("a = b", is_incomplete_function_callsite)
    negative_test("lambda a: a ** 2", is_incomplete_function_callsite)
    negative_test("t = (a, b, c", is_incomplete_function_callsite)
    negative_test("x = baz.bar.foo(a, b, c)", is_incomplete_function_callsite)


def test_is_keyword_argument():
    root = parso.parse("foo(key=value)")
    arg = root.children[0].children[1].children[1]
    assert is_keyword_argument(arg)

    root = parso.parse("foo(value)")
    arg = root.children[0].children[1].children[1]
    assert not is_keyword_argument(arg)


def test_is_incomplete_keyword_argument():
    root = parso.parse("foo(key=")
    arg = root.children[0].children[-2]
    assert is_incomplete_keyword_argument(arg)

    root = parso.parse("foo(value")
    arg = root.children[0].children[-1]
    assert not is_incomplete_keyword_argument(arg)


def test_get_keyword():
    root = parso.parse("foo(key=value)")
    arg = root.children[0].children[1].children[1]
    assert get_keyword(arg).value == "key"

    root = parso.parse("foo(key=")
    arg = root.children[0].children[-2]
    assert get_keyword(arg).value == "key"


def test_callargs():
    def common_test(text: Text, expected: List[str]):
        """
        Assert the callargs at the function callsite in @text are @expected.

        :param text: source text to parse
        :param expected: list of expected callargs in @text
        """
        root = parso.parse(text)
        call = root.children[0]
        args = [arg.value for arg in callargs(call)]
        assert args == expected

    common_test("foo(", [])
    common_test("foo()", [])
    common_test("foo(a", ["a"])
    common_test("foo(a)", ["a"])
    common_test("foo(a, b, c", ["a", "b", "c"])
    common_test("foo(a, b, c)", ["a", "b", "c"])


def test_normalize_callarg():
    def common_test(text: Text, expected: List[str]):
        """
        Assert the normalized callargs at the function callsite in @text
        are @expected.

        :param text: source text to parse
        :param expected: list of expected callargs in @text
        """
        root = parso.parse(text)
        call = root.children[0]
        args = [normalize_callarg(arg).value for arg in callargs(call)]
        assert args == expected

    common_test("foo(a)", ["a"])
    common_test("foo(value=a)", ["a"])
    common_test("foo(obj.a)", ["a"])
    common_test("foo(value=obj.a)", ["a"])


def test_in_scope_imports():
    root = parso.parse(slurp(DATA_DIR / "imports1.py"))
    leaf = root.get_last_leaf().get_previous_leaf()
    assert len(in_scope_imports(leaf)) == 2

    root = parso.parse(slurp(DATA_DIR / "imports2.py"))
    leaf = root.get_last_leaf().get_previous_leaf()
    assert len(in_scope_imports(leaf)) == 1


def test_in_scope_import_froms():
    root = parso.parse(slurp(DATA_DIR / "import_froms1.py"))
    leaf = root.get_last_leaf().get_previous_leaf()
    assert len(in_scope_import_froms(leaf)) == 2

    root = parso.parse(slurp(DATA_DIR / "import_froms2.py"))
    leaf = root.get_last_leaf().get_previous_leaf()
    assert len(in_scope_import_froms(leaf)) == 1


def test_import_aliases():
    root = parso.parse(slurp(DATA_DIR / "imports1.py"))
    leaf = root.get_last_leaf().get_previous_leaf()
    assert _import_aliases(leaf) == {"path": "os.path", "regex": "re"}

    root = parso.parse(slurp(DATA_DIR / "imports2.py"))
    leaf = root.get_last_leaf().get_previous_leaf()
    assert _import_aliases(leaf) == {"re": "re"}


def test_import_names():
    root = parso.parse(slurp(DATA_DIR / "import_froms1.py"))
    leaf = root.get_last_leaf().get_previous_leaf()
    assert _import_names(leaf) == {"join": "os.path", "match": "re"}

    root = parso.parse(slurp(DATA_DIR / "import_froms2.py"))
    leaf = root.get_last_leaf().get_previous_leaf()
    assert _import_names(leaf) == {"match": "re"}


def test_callsite_routines():
    def os_path_join_test(path: Path):
        """
        Assert the `os.path.join` function callsite in the file
        at @path can be successfully queried by the callsite
        routines.

        :param path: path to file containing source text
        """
        script = Script(slurp(path))
        root = script._module_node
        callsite = (
            root.children[-2].children[0]
            if isinstance(root.children[-2], PythonNode)
            else root.children[-2]
        )

        assert callsite_signature(script, callsite)
        assert callsite_module(script, callsite) == "os.path"
        assert callsite_function(script, callsite) == "join"

    def foo_bar_baz_test(path: Path):
        """
        Assert the `foo.bar.baz` function callsite in the file
        at PATH can be successfully queried by the callsite
        routines.

        :param path: path to file containing source text
        """
        script = Script(slurp(path))
        root = script._module_node
        callsite = (
            root.children[-2].children[0]
            if isinstance(root.children[-2], PythonNode)
            else root.children[-2]
        )

        assert callsite_signature(script, callsite) is None
        assert callsite_module(script, callsite) == "foo.bar"
        assert callsite_function(script, callsite) == "baz"

    os_path_join_test(DATA_DIR / "os_path_join_callsite1.py")
    os_path_join_test(DATA_DIR / "os_path_join_callsite2.py")
    os_path_join_test(DATA_DIR / "os_path_join_callsite3.py")
    os_path_join_test(DATA_DIR / "os_path_join_callsite4.py")
    os_path_join_test(DATA_DIR / "os_path_join_callsite5.py")
    os_path_join_test(DATA_DIR / "os_path_join_callsite6.py")
    os_path_join_test(DATA_DIR / "os_path_join_callsite7.py")
    os_path_join_test(DATA_DIR / "os_path_join_callsite8.py")
    os_path_join_test(DATA_DIR / "os_path_join_callsite9.py")
    os_path_join_test(DATA_DIR / "os_path_join_callsite10.py")
    os_path_join_test(DATA_DIR / "os_path_join_callsite11.py")
    os_path_join_test(DATA_DIR / "os_path_join_callsite12.py")
    os_path_join_test(DATA_DIR / "os_path_join_callsite13.py")
    os_path_join_test(DATA_DIR / "os_path_join_callsite14.py")
    os_path_join_test(DATA_DIR / "os_path_join_callsite15.py")
    os_path_join_test(DATA_DIR / "os_path_join_callsite16.py")
    os_path_join_test(DATA_DIR / "os_path_join_callsite17.py")
    os_path_join_test(DATA_DIR / "os_path_join_callsite18.py")
    os_path_join_test(DATA_DIR / "os_path_join_callsite19.py")
    os_path_join_test(DATA_DIR / "os_path_join_callsite20.py")

    foo_bar_baz_test(DATA_DIR / "foo_bar_baz_callsite1.py")
    foo_bar_baz_test(DATA_DIR / "foo_bar_baz_callsite2.py")
    foo_bar_baz_test(DATA_DIR / "foo_bar_baz_callsite3.py")
    foo_bar_baz_test(DATA_DIR / "foo_bar_baz_callsite4.py")
    foo_bar_baz_test(DATA_DIR / "foo_bar_baz_callsite5.py")
    foo_bar_baz_test(DATA_DIR / "foo_bar_baz_callsite6.py")
    foo_bar_baz_test(DATA_DIR / "foo_bar_baz_callsite7.py")
    foo_bar_baz_test(DATA_DIR / "foo_bar_baz_callsite8.py")
    foo_bar_baz_test(DATA_DIR / "foo_bar_baz_callsite9.py")
    foo_bar_baz_test(DATA_DIR / "foo_bar_baz_callsite10.py")
    foo_bar_baz_test(DATA_DIR / "foo_bar_baz_callsite11.py")
    foo_bar_baz_test(DATA_DIR / "foo_bar_baz_callsite12.py")
    foo_bar_baz_test(DATA_DIR / "foo_bar_baz_callsite13.py")
    foo_bar_baz_test(DATA_DIR / "foo_bar_baz_callsite14.py")
    foo_bar_baz_test(DATA_DIR / "foo_bar_baz_callsite15.py")
    foo_bar_baz_test(DATA_DIR / "foo_bar_baz_callsite16.py")
    foo_bar_baz_test(DATA_DIR / "foo_bar_baz_callsite17.py")


def test_in_scope_names():
    def test_node(root: Module) -> BaseNode:
        """
        Retrieve the node with the text 'print("TEST_NODE")' in @root.

        :param root: root AST node
        :return: node with the text 'print("TEXT_NODE")' in @root
        """
        TEXT = 'print("TEST_NODE")'
        node = filter_asts(lambda a: a.get_code().strip() == TEXT, root)[0]
        return node

    def common_test(text: Text, expected: List[str]):
        """
        Assert the names in scope at the "TEST_PT" in @text are @expected.

        :param text: source text containing a "TEST_PT"
        :param expected: list of names expected to be in scope at "TEST_PT"
        """
        root = parso.parse(text)
        node = test_node(root)
        names = in_scope_names(node)
        names = [name for name in names if not name.startswith("__")]
        assert names == expected

    common_test(slurp(DATA_DIR / "in_scope_names_nonlocal1.py"), ["a"])
    common_test(slurp(DATA_DIR / "in_scope_names_nonlocal2.py"), ["a", "b"])
    common_test(slurp(DATA_DIR / "in_scope_names_global1.py"), ["a"])
    common_test(slurp(DATA_DIR / "in_scope_names_global2.py"), ["a", "b"])
    common_test(slurp(DATA_DIR / "in_scope_names_assign1.py"), ["a"])
    common_test(slurp(DATA_DIR / "in_scope_names_assign2.py"), ["a"])
    common_test(slurp(DATA_DIR / "in_scope_names_assign3.py"), ["a"])
    common_test(slurp(DATA_DIR / "in_scope_names_assign4.py"), ["a", "b"])
    common_test(slurp(DATA_DIR / "in_scope_names_import_from1.py"), ["a"])
    common_test(slurp(DATA_DIR / "in_scope_names_function1.py"), ["foo", "a"])
    common_test(slurp(DATA_DIR / "in_scope_names_function2.py"), ["foo", "a", "b", "c"])
    common_test(slurp(DATA_DIR / "in_scope_names_function3.py"), ["foo", "a", "b", "c"])
    common_test(slurp(DATA_DIR / "in_scope_names_lambda1.py"), ["a"])
    common_test(slurp(DATA_DIR / "in_scope_names_class1.py"), ["Foo"])
    common_test(slurp(DATA_DIR / "in_scope_names_for1.py"), ["i"])
    common_test(slurp(DATA_DIR / "in_scope_names_for2.py"), ["i"])
    common_test(slurp(DATA_DIR / "in_scope_names_for3.py"), ["r", "i"])
    common_test(slurp(DATA_DIR / "in_scope_names_with1.py"), ["a"])
    common_test(slurp(DATA_DIR / "in_scope_names_with2.py"), ["a"])
    common_test(slurp(DATA_DIR / "in_scope_names_try1.py"), ["a", "e"])
    common_test(slurp(DATA_DIR / "in_scope_names_try2.py"), ["a", "e"])
    common_test(slurp(DATA_DIR / "in_scope_names_try3.py"), ["a"])
    common_test(slurp(DATA_DIR / "in_scope_names_try4.py"), ["a"])
    common_test(slurp(DATA_DIR / "in_scope_names_if1.py"), ["e"])
    common_test(slurp(DATA_DIR / "in_scope_names_if2.py"), ["a"])
    common_test(slurp(DATA_DIR / "in_scope_names_named_expression1.py"), ["a"])
    common_test(slurp(DATA_DIR / "in_scope_names_named_expression2.py"), ["a"])
    common_test(slurp(DATA_DIR / "in_scope_names_named_expression3.py"), ["a"])
