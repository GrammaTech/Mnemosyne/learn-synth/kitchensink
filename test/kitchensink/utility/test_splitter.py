from kitchensink.utility.splitter import PythonHeuristicSplitter, PythonRoninSplitter


def test_python_heuristic_splitter():
    splitter = PythonHeuristicSplitter()

    assert ["a"] == splitter.split("a")
    assert ["a"] == splitter.split("a1")
    assert ["min", "x"] == splitter.split("min_x")
    assert ["xmin"] == splitter.split("xmin")
    assert ["abs", "min", "x"] == splitter.split("abs_min_x")
    assert ["min", "X"] == splitter.split("minX")
    assert ["abs", "Min", "X"] == splitter.split("absMinX")
    assert ["name"] == splitter.split("_name")
    assert ["name"] == splitter.split("name8")
    assert ["__name__"] == splitter.split("__name__")
    assert ["utf8", "encoding"] == splitter.split("utf8_encoding")
    assert ["Convert", "To", "A", "UTF8", "String"] == splitter.split(
        "ConvertToAUTF8String"
    )


def test_python_ronin_splitter():
    splitter = PythonRoninSplitter()

    assert ["a"] == splitter.split("a")
    assert ["a"] == splitter.split("a1")
    assert ["min", "x"] == splitter.split("min_x")
    assert ["x", "min"] == splitter.split("xmin")
    assert ["abs", "min", "x"] == splitter.split("abs_min_x")
    assert ["min", "X"] == splitter.split("minX")
    assert ["abs", "Min", "X"] == splitter.split("absMinX")
    assert ["name"] == splitter.split("_name")
    assert ["name"] == splitter.split("name8")
    assert ["__name__"] == splitter.split("__name__")
    assert ["utf8", "encoding"] == splitter.split("utf8_encoding")
    assert ["Convert", "To", "A", "UTF8", "String"] == splitter.split(
        "ConvertToAUTF8String"
    )
