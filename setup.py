import setuptools

with open("README.md", "r") as f:
    long_description = f.read()

setuptools.setup(
    name="kitchensink",
    author="Grammatech",
    description="Collection of utilities to be shared across learn-synth repositories.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/kitchensink",
    packages=["kitchensink.utility"],
    package_dir={"": "src"},
    python_requires=">=3.6",
    classifiers=[
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    install_requires=[
        "asts",
        "cchardet",
        "jedi",
        "pygls",
        "pygments",
        "zodb",
        "RelStorage[sqlite3]",
        "spiral @ git+https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/spiral.git@master",
    ],
    extras_require={"dev": ["black", "flake8", "pre-commit", "pytest"]},
)
