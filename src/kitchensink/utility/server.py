"""
Collection of utilities for simplifying the definition of LSP servers.
"""

import argparse


def lsp_arg_parser(desc: str, default_port: int, default_stdio_flag=False):
    """
    Returns an arg parser that includes the standard LSP server args `port` and `stdio`.
    """
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument(
        "--port",
        type=int,
        default=default_port,
        help="Port to listen for requests on.",
    )
    parser.add_argument(
        "--stdio",
        action="store_true",
        default=default_stdio_flag,
        help="Run server in STDIO mode.",
    )
    return parser


def lsp_arg_parser_with_rest(
    desc: str, default_port: int, default_stdio_flag=False, default_rest_flag=False
):
    """
    Like `lsp_arg_parser` but the returned arg parser has an additional param `rest`
    which specifies that a REST protocol should be used when it is set.
    """
    parser = lsp_arg_parser(desc, default_port, default_stdio_flag)
    parser.add_argument(
        "--rest",
        default=default_rest_flag,
        action="store_true",
        help="Flag to start the server as a REST web service.",
    )
    return parser
