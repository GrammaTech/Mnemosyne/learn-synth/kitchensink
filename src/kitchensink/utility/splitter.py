from abc import ABC, abstractmethod
from typing import Callable, List
import spiral


class Splitter(ABC):
    """
    an abstract base class for name splitting strategy
    """

    @abstractmethod
    def _splitter(self) -> Callable[[str], List[str]]:
        """
        splitting algorithm to be utilized
        """
        raise NotImplementedError

    def split(self, name: str) -> List[str]:
        """
        split a name into constituent terms

        :param name: name to be split
        :return: list of terms
        """
        return [
            term for term in self._splitter()(name) if any(c.isalpha() for c in term)
        ]

    def split_lower(self, name: str) -> List[str]:
        """
        split a name into lowercased constituent terms

        :param name: name to be split
        :return: list of lowercased terms
        """
        return [term.lower() for term in self.split(name)]


class HeuristicSplitter(Splitter):
    """
    heuristic splitter from spiral, based on underscores and casing
    """

    def _splitter(self):
        return spiral.heuristic_split


class RoninSplitter(Splitter):
    """
    ronin splitter from spiral, based on an advanced frequency-based algorithm
    """

    def _splitter(self):
        return spiral.ronin.split


class PythonSplitter(Splitter):
    """
    a splitter for python preserving special variables names
    while using a splitting algorithm otherwise
    """

    def split(self, name: str) -> List[str]:
        SPECIAL_VARIABLES = {
            "__all__",
            "__annotations__",
            "__closure__",
            "__code__",
            "__defaults__",
            "__dict__",
            "__doc__",
            "__file__",
            "__globals__",
            "__kwdefaults__",
            "__name__",
            "__module__",
            "__package__",
            "__qualname__",
            "__spec__",
        }

        if name in SPECIAL_VARIABLES:
            return [name]
        else:
            return super().split(name)


class PythonHeuristicSplitter(PythonSplitter, HeuristicSplitter):
    pass


class PythonRoninSplitter(PythonSplitter, RoninSplitter):
    pass
