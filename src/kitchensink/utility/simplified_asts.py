"""
Utilities to convert ASTs to their simplified form,
as described in "Example Gratis (E.G.): Code Examples
for Free" section 4.1 or "Aroma: Code Recommendation
via Structural Code Search" section 3.1.
"""

from dataclasses import dataclass, field
from typing import Dict, Generator, List, Optional, Text, Tuple, TypeVar, Union
from kitchensink.utility.parso import normalize_quotes
from pygments.lexer import Lexer
from pygments.lexers import CLexer, CppLexer, JavascriptLexer, PythonLexer
from pygments.token import Token, _TokenType
from asts import (
    AST,
    ASTLanguage,
    ArgumentsAST,
    CallAST,
    CatchAST,
    CompoundAST,
    FieldAST,
    FunctionAST,
    LambdaAST,
    IdentifierAST,
    StatementAST,
    StringAST,
    SubscriptAST,
    VariableDeclarationAST,
)


DictOrList = Union[List, Dict]
ASTPath = Tuple[int]
SimplifiedASTType = TypeVar("SimplifiedASTType", bound="SimplifiedAST")
TokenTuple = Tuple[int, _TokenType, str]


class SimplifiedAST:
    """
    data structure representing a simplified AST for
    use in the E.G. algorithm
    """

    def __init__(
        self,
        data: DictOrList,
        path: ASTPath = (),
        parent: Optional[SimplifiedASTType] = None,
        children: List[SimplifiedASTType] = [],
        marked: bool = False,
    ):
        """
        create a simplified AST representation

        :param data: the data (label or token) for this node
        :param path: path from the root of the tree to this node
        :param parent: parent of this node
        :param children: child nodes below this node
        :param marked: flag to denote if this node is marked
        """
        self._data = data
        self._path = path
        self._parent = parent
        self._children = children
        self._marked = marked

    @property
    def data(self) -> DictOrList:
        """
        return the data field of the simplified AST
        """
        return self._data

    @property
    def path(self) -> ASTPath:
        """
        return the path from the root to this simplified AST
        """
        return self._path

    @property
    def parent(self) -> SimplifiedASTType:
        """
        return the parent of this simplified AST
        """
        return self._parent

    @property
    def children(self) -> List[SimplifiedASTType]:
        """
        return the children of this simplified AST
        """
        return self._children

    @property
    def marked(self) -> bool:
        """
        return a boolean representing if this simplified AST has
        been marked
        """
        return self._marked

    @property
    def size(self) -> int:
        """
        return the number of nodes in this simplified AST
        """
        if "size" not in self.__dict__:
            self.__dict__["size"] = 1 + sum(child.size for child in self.children)
        return self.__dict__["size"]

    @property
    def leaf(self) -> bool:
        """
        return the value of the "leaf" field on the simplified AST
        """
        return self.data.get("leaf", False) if isinstance(self.data, dict) else False

    def traverse(self) -> Generator[SimplifiedASTType, None, None]:
        """
        traverse the simplified AST in pre-order, yielding subtrees
        as appropriate
        """
        yield self
        for child in self.children:
            yield from child.traverse()

    def token_or_label(self) -> str:
        """
        return the token or label representing this simplified AST
        """
        if isinstance(self.data, dict):
            return self.data["token"]
        elif isinstance(self.data, str):
            return self.data

    def text(self, prettify=True) -> Text:
        """
        return the source text of this simplified AST

        :param prettify: replace tabs with spaces and remove extraneous
                         leading tabs if true
        """

        result = ""

        for node in self.traverse():
            if not node.children:
                result += node.data["leading"]
                result += node.data["token"]
                result += node.data["trailing"]

        return SimplifiedAST._prettify(result) if prettify else result

    def snippet(self, prettify=True) -> Text:
        """
        return a textmate snippet from this simplified AST

        :param prettify: replace tabs with spaces and remove extraneous
                         leading tabs if true
        """

        def is_placeholder_root(node: SimplifiedASTType) -> bool:
            """
            return true if @node is the root of a placeholder in the snippet

            :param node: node in the simplified AST tree
            """
            parent = node.parent

            if node.marked and node.children:
                # This is the root of a placeholder tree if the
                # node is marked but none of its children are.
                # This is a bit of a special case to consolidate
                # multiple unmarked children into a single
                # placeholder.
                return not any(c.marked for c in node.children)
            elif not node.marked and parent and parent.marked:
                # This is the root of a placeholder tree if
                # the node is marked but the parent is not
                # and at least one of the parent's children
                # is marked.
                return any(c.marked for c in parent.children)
            else:
                # This is not a placeholder tree.
                return False

        def placeholder_root(node: SimplifiedASTType) -> Optional[SimplifiedASTType]:
            """
            return the root of the placeholder in the snippet containing node,
            if possible

            :param node: node in the simplified AST tree
            """
            while node and not is_placeholder_root(node):
                node = node.parent
            return node

        def first_placeholder_leaf(node: SimplifiedASTType) -> bool:
            """
            return tree if node is the first leaf of a placeholder in a snippet

            :param node: node in the simplified AST tree
            """
            root = placeholder_root(node)

            # Checks to make sure this is a leaf (1), in a placeholder
            # tree (2), and not a standalone operator (3).
            result = not node.children  # (1)
            result &= root is not None  # (2)
            result &= node != root or node.data.get("leaf", False)  # (3)

            # Check to ensure this is the first leaf.
            if result:
                path = root.relative_path(node)
                result &= all(i == 0 for i in path)

            return result

        def last_placeholder_leaf(node: SimplifiedASTType) -> bool:
            """
            return tree if node is the last leaf of a placeholder in a snippet

            :param node: node in the simplified AST tree
            """
            root = placeholder_root(node)

            # Checks to make sure this is a leaf (1), in a placeholder
            # tree (2), and not a standalone operator (3).
            result = not node.children  # (1)
            result &= root is not None  # (2)
            result &= node != root or node.data.get("leaf", False)  # (3)

            # Check to ensure this is the last leaf.
            if result:
                path = root.relative_path(node)
                for i in range(len(path)):
                    children = root.get(path[:i]).children
                    result &= len(children) == path[i] + 1

            return result

        result = ""
        placeholder_index = 0

        for node in self.traverse():
            if not node.children:
                result += node.data["leading"]
                if first_placeholder_leaf(node):
                    placeholder_index += 1
                    result += "${" + str(placeholder_index) + ":"
                result += node.data["token"]
                if last_placeholder_leaf(node):
                    result += "}"
                result += node.data["trailing"]

        return SimplifiedAST._prettify(result) if prettify else result

    def get(self, path: ASTPath) -> SimplifiedASTType:
        """
        return the node in this simplified AST at @path

        :param path: tuple of integers representing child indices
            to traverse to retrieve the node
        """
        if not path:
            return self
        else:
            return self.children[path[0]].get(path[1:])

    def relative_path(self, node: SimplifiedASTType) -> ASTPath:
        """
        return the relative path from self to @node.
        if self is a child of @node, the path is reversed
        and the child indices are represented as negative
        integers

        :param node: other node in the same tree as self
        """
        if len(self.path) <= len(node.path):
            return node.path[len(self.path) :]
        else:
            return tuple(-i for i in reversed(node.relative_path(self)))

    def mark(self) -> None:
        """
        set the marked flag on this simplified AST to TRUE
        """
        self._marked = True

    def unmark(self) -> None:
        """
        set the marked flag on this simplified AST to FALSE
        """
        self._marked = False

    def to_json(self) -> DictOrList:
        """
        convert self to a form which may be serialized as JSON
        """
        result = [self.data] if isinstance(self.data, str) else self.data
        for child in self.children:
            result.append(child.to_json())
        return result

    @staticmethod
    def _prettify(source: Text) -> Text:
        """
        replace tabs with spaces and remove extraneous leading tabs
        from @source

        :param source: source text to prettify
        """
        lines = source.replace("\t", "    ").split("\n")

        # Find the amount of extraneous leading whitespace on each line.
        ws_len = [ln.find(ln.lstrip()) for ln in lines if ln.strip()]
        ws_len = min(ws_len) if ws_len else 0

        # Remove extraneous leading whitespace from each line.
        results = []
        for line in lines:
            results.append(line[ws_len:].rstrip())

        # Remove extraneous leading empty lines.
        while results and not results[0].strip():
            results.pop(0)

        # Remove extraneous trailing empty lines.
        while results and not results[-1].strip():
            results.pop()

        # Join the resulting lines with a newline and return.
        return "\n".join(results)

    @staticmethod
    def from_json(json: List) -> SimplifiedASTType:
        """
        create a simplified AST object from the @json representation

        :param json: json representation of a simplified AST
        """

        def helper(
            json: DictOrList,
            path: ASTPath = (),
            parent: Optional[SimplifiedASTType] = None,
        ) -> SimplifiedASTType:
            data = json[0] if isinstance(json, list) else json
            children = json[1:] if isinstance(json, list) else []

            tree = SimplifiedAST(data=data, path=path, parent=parent)
            tree._children = [
                helper(c, path=path + (i,), parent=tree) for i, c in enumerate(children)
            ]

            return tree

        return helper(json)

    @staticmethod
    def from_sel(root: AST, function: AST) -> Optional[SimplifiedASTType]:
        """
        create a simplified AST object from the SEL @function representation

        :param root: SEL root of the AST containing @function
        :param function: SEL function AST to convert
        """

        assert isinstance(
            function, FunctionAST
        ), "The AST to simplify must be a function node."
        assert not isinstance(
            function, LambdaAST
        ), "The AST to simplify must be a function node."

        TokenTreeType = TypeVar("SimplifiedASTType", bound="SimplifiedAST")

        @dataclass
        class TokenTree:
            """
            Representation of a tree of lexer tokens.
            """

            data: Optional[TokenTuple] = None
            prefix: Text = ""
            suffix: Text = ""
            parent: Optional[TokenTreeType] = None
            children: List[TokenTreeType] = field(default_factory=list)

        def build_token_tree(function: AST) -> TokenTreeType:
            """
            Build a tree of lexer tokens from the given parsed @function.
            The simplified AST format requires a finer level of detail than
            is provided in tree-sitter ASTs, necessitating the use of lexer
            tokens.

            :param body: function AST node
            """

            def location_to_offset(
                location: Tuple[int, int],
                line_lengths: List[int],
            ) -> int:
                """
                Convert the given line, column location to an integer offset.

                :param location: (line, column) tuple where line and column
                    are 1-indexed
                :param line_lengths: lengths of the lines containing location
                """
                line, column = location
                line, column = line - 1, column - 1
                return sum(length + 1 for length in line_lengths[:line]) + column

            def get_lexer(language: ASTLanguage) -> Lexer:
                """
                Return a lexer capable of lexing the given source language.

                :param language: source language
                """

                if language == ASTLanguage.Python:
                    return PythonLexer()
                elif language == ASTLanguage.Javascript:
                    return JavascriptLexer()
                elif language == ASTLanguage.C:
                    return CLexer()
                elif language == ASTLanguage.Cpp:
                    return CppLexer()

                raise RuntimeError(f"{language} is not supported.")

            def tokens_in_range(
                rnge: Tuple[int, int],
                tokens: List[TokenTuple],
            ) -> List[TokenTuple]:
                """
                Return the list of tokens in the given range.

                :param rnge: (start_offset, end_offset) tuple
                :param tokens: sorted list of tokens
                """
                start, end = rnge

                results = []
                for token in tokens:
                    token_start, _, text = token
                    token_end = token_start + len(text)
                    if start <= token_start and token_end <= end:
                        results.append(token)
                    if end < token_start:
                        break

                return results

            def build_ast_ranges(
                node: AST,
                line_lengths: List[int],
            ) -> Dict[int, Tuple[int, int]]:
                """
                Return a dictionary mapping @node and its recursive children
                to (start_offset, end_offset) tuples.

                :param node: generic AST node
                :param line_lengths: pre-computed length of each line in @node's
                    source text
                """
                return {
                    ast: (
                        location_to_offset(rnge[0], line_lengths),
                        location_to_offset(rnge[1], line_lengths),
                    )
                    for ast, rnge in node.ast_source_ranges()
                }

            def token_tree_children(node: AST) -> List[AST]:
                """
                Return the children of @node to be used in the token tree.

                :param node: generic AST node
                """
                if isinstance(node, (FieldAST, StringAST)):
                    # Special case, for fields (e.g. "a.b") and strings
                    # don't recurse further down into the children.  For
                    # fields, this simplifies the tree and allows us
                    # to perform fixups for import aliases below.  For strings,
                    # this avoids adding text fragment ASTs for escaped
                    # unicode sequences to the tree.
                    return []
                else:
                    return node.children

            def ast_subranges(
                node: AST,
                ast_ranges: Dict[int, Tuple[int, int]],
            ) -> List[Tuple[Optional[AST], Tuple[int, int]]]:
                """
                Return the subranges of @node - the start/end offets
                of each child and the interleaving text between children.

                :param node: AST to find subranges of
                :param ast_ranges: dictionary mapping ASTs to start/stop offsets
                """
                start_pos, end_pos = ast_ranges[node]

                results = []
                for child in token_tree_children(node):
                    child_start, child_end = ast_ranges[child]
                    results.append((None, (start_pos, child_start)))
                    results.append((child, (child_start, child_end)))
                    start_pos = child_end
                results.append((None, (start_pos, end_pos)))

                return results

            def build_token_groups(
                node: AST,
                tokens: List[TokenTuple],
                ast_ranges: Dict[int, Tuple[int, int]],
            ) -> List[Tuple[Optional[AST], List[TokenTuple]]]:
                """
                Return a list of token groups representing the simplified
                AST children of @node.

                :param node: generic AST node
                :param tokens: list of tokens in @node's source range
                :param ast_ranges: dictionary mapping ASTs to start/stop offsets
                """
                results = []

                for child_ast, subrange in ast_subranges(node, ast_ranges):
                    subtokens = tokens_in_range(subrange, tokens)
                    results.append((child_ast, subtokens))

                return results

            def create_filler_node(children: TokenTreeType) -> TokenTreeType:
                """
                Create a filler (empty) node in the token tree which is a parent of children.

                :param children: list of generic token tree children
                """
                result = TokenTree()

                for child in children:
                    result.parent = child.parent
                    child.parent = result
                result.children = children

                return result

            def create_keyword_statement_node(
                token_tree: TokenTreeType,
                node: AST,
            ) -> TokenTreeType:
                """
                For statement ASTs where the first child is a keyword (e.g.
                return), create an additional filler node parent.  This ensures
                the simplified AST labels for compound statements (e.g. loop/if
                statement bodies) do not include keywords in them, which could
                affect pattern creation in the Exempla Gratis algorithm.

                :param token_tree: non-leaf token tree node
                :param node: corresponding SEL AST node
                """

                def has_statement_children(node: AST) -> bool:
                    """
                    Return TRUE if any children of node (recursive) contain a
                    statement AST.

                    :param node: generic AST node
                    """
                    return any(
                        isinstance(c, (StatementAST, CompoundAST))
                        for c in node.children
                    )

                def is_keyword_statement(token_tree: TokenTreeType, node: AST) -> bool:
                    """
                    Return TRUE if @token_tree represents a statement AST where the
                    first child is a keyword.

                    :param token_tree: non-leaf token tree node
                    :param node: corresponding SEL AST node
                    """
                    return (
                        isinstance(node, StatementAST)
                        and not has_statement_children(node)
                        and token_tree.children
                        and token_tree.children[0].data
                        and Token.Keyword in token_tree.children[0].data[1].split()
                    )

                if is_keyword_statement(token_tree, node):
                    return create_filler_node(children=[token_tree])
                else:
                    return token_tree

            def create_multiple_arguments_node(
                token_tree: TokenTreeType,
                node: AST,
            ) -> TokenTreeType:
                """
                For argument list ASTs with multiple args, create a filler node
                parent above the argument.  This ensures parenthesis are always
                included in the fixed portion of the pattern generated by
                the Exempla Gratis algorithm.  Further, it matches the simplified
                AST forms displayed in figure 8 of the Exempla Gratis paper.

                :param token_tree: non-leaf token tree node
                :param node: corresponding SEL AST node
                """

                def is_multiple_argument_arglist(
                    token_tree: TokenTreeType, node: AST
                ) -> bool:
                    """
                    Return TRUE if @token_tree represents an argument list with more than
                    one argument.

                    :param token_tree: non-leaf token tree node
                    :param node: corresponding SEL AST node
                    """
                    return (
                        isinstance(node, ArgumentsAST)
                        and len(token_tree.children[1:-1]) > 1
                    )

                if is_multiple_argument_arglist(token_tree, node):
                    args = token_tree.children[1:-1]
                    args = create_filler_node(children=args)
                    token_tree.children = [
                        token_tree.children[0],
                        args,
                        token_tree.children[-1],
                    ]
                    return token_tree
                else:
                    return token_tree

            def create_subscript_node(
                token_tree: TokenTreeType,
                node: AST,
            ) -> TokenTreeType:
                """
                For array accesses, create a filler node parent above the subscript.
                This assists Exempla Gratis pattern creation by removing subscript
                delimiters from the simplified AST labels.

                :param token_tree: non-leaf token tree node
                :param node: corresponding SEL AST node
                """

                def is_subscript(token_tree: TokenTreeType, node: AST) -> bool:
                    """
                    Return TRUE if @token_tree represents an array access

                    :param token_tree: non-leaf token tree node
                    :param node: corresponding SEL AST node
                    """
                    return (
                        isinstance(node, SubscriptAST)
                        and len(token_tree.children) >= 3
                        and token_tree.children[-3].data
                        and token_tree.children[-3].data[2] == "["
                        and token_tree.children[-1].data
                        and token_tree.children[-1].data[2] == "]"
                    )

                if is_subscript(token_tree, node):
                    subscript = token_tree.children[-3:]
                    subscript = create_filler_node(children=subscript)
                    value = token_tree.children[:-3]
                    token_tree.children = value + [subscript]
                    return token_tree
                else:
                    return token_tree

            def flatten_catch_asts(
                token_tree: TokenTreeType,
                node: AST,
            ) -> List[TokenTreeType]:
                """
                For catch ASTs, split the exception catch clause and the handler
                body into separate ASTs and remove the parent AST.

                :param token_tree: non-leaf token tree node
                :param node: corresponding SEL AST node
                """
                if isinstance(node, CatchAST):
                    clause = token_tree.children[:-1]
                    body = token_tree.children[-1]
                    return [create_filler_node(children=clause), body]
                else:
                    return [token_tree]

            def combine_string_tokens(
                node: Optional[AST],
                tokens: List[TokenTuple],
            ) -> List[TokenTuple]:
                """
                Combine string literal tokens in @tokens into a single token.

                :param node: generic AST node
                :param tokens: list of tokens in @node's source range
                """

                def is_string_token(token: TokenTuple) -> bool:
                    """
                    Return true if @token is a string lexer token.

                    :param token: lexer token
                    """
                    _1, token_type, _2 = token
                    for t in token_type.split():
                        if t == Token.Literal.String:
                            return True
                    return False

                def should_combine_token(
                    token: TokenTuple,
                    node: Optional[AST],
                ) -> bool:
                    """
                    Return TRUE if @token should be combined into a larger token.

                    :param token: lexer token
                    :param node: generic AST node containing token
                    """
                    if (
                        isinstance(node, StringAST)
                        and Token.Text not in token[1].split()
                    ):
                        return True
                    elif is_string_token(token):
                        return True
                    else:
                        return False

                result = []
                combined_string_token = None

                for token in tokens:
                    if should_combine_token(token, node):
                        if combined_string_token is None:
                            combined_string_token = token
                        else:
                            position, token_type, text = combined_string_token
                            combined_string_token = (
                                position,
                                Token.Literal.String,
                                text + token[2],
                            )
                    else:
                        if combined_string_token:
                            result.append(combined_string_token)
                            combined_string_token = None
                        result.append(token)

                if combined_string_token:
                    result.append(combined_string_token)

                return result

            def callsite_tokens(
                node: Optional[AST],
                tokens: List[TokenTuple],
            ) -> List[TokenTuple]:
                """
                If @node is a function callsite, return list of tokens to
                represent the callsite in the token tree.

                Generally we want to ensure consistency in tokens here such
                that calls to the same function and represented with the same
                tokens in a simplified AST tree.

                :param node: generic AST node
                :param tokens: list of tokens in @node's source range
                """

                def is_library_function(node: AST) -> bool:
                    """
                    Return true if @node is a call to an external library.

                    :param node: generic AST node
                    """
                    if not node:
                        return False
                    if not isinstance(node, (IdentifierAST, FieldAST)):
                        return False

                    parent = node.parent(root)
                    if not isinstance(parent, CallAST):
                        return False
                    if node != parent.call_function():
                        return False

                    provided_by = parent.provided_by(root)
                    if not provided_by or provided_by == "builtins":
                        return False
                    return True

                def python_library_function_tokens(
                    node: AST,
                    tokens: List[TokenTuple],
                ) -> List[TokenTuple]:
                    """
                    Return a list of tokens representing the given python
                    library function call at @node.

                    For python ASTs, callsites always use the fully qualified,
                    unaliased module name and function.  This ensures calls to
                    the same API are represented as simplified ASTs in the same
                    manner.  For instance, `json.dump`, `<json-alias>.dump`, and
                    `dump` are all stored as `json.dump` regardless of import
                    aliases or names included in import-from statements

                    :param node: python library function call node
                    :param tokens: list of tokens in @node's source range
                    """
                    result = []

                    # Prepend leading whitespace to the result.
                    for t in tokens:
                        _, token_type, text = t
                        if Token.Text in token_type.split():
                            result.append([None, token_type, text])

                    # Add the fully qualified, unaliased name of the library
                    # to the list of tokens.
                    for t in node.parent(root).provided_by(root).split("."):
                        result.append([None, Token.Name, t])
                        result.append([None, Token.Operator, "."])

                    # Append the function name token and any other trailing
                    # whitespace tokens to the result.
                    trailer = []
                    for t in reversed(tokens):
                        _, token_type, text = t
                        trailer.append([None, token_type, text])
                        if token_type == Token.Name:
                            break
                    result.extend(reversed(trailer))

                    return result

                if (
                    node
                    and node.language == ASTLanguage.Python
                    and is_library_function(node)
                ):
                    return python_library_function_tokens(node, tokens)

                return tokens

            def group_fields(children: List[TokenTreeType]) -> List[TokenTreeType]:
                """
                Group field accesses in @children such that each dotted name
                appears in a group under a filler AST.  For instance, "a.b.c"
                would be represented as:

                      Parent AST
                      |   |    |
                      a   x    x
                         /|   /|
                        . b  . c

                This improves the pattern matching abilities of the Exempla
                Gratis algorithm by removing periods from the labels of
                field.  Further, it matches the simplified AST forms
                displayed in figure 8 of the Exempla Gratis paper.

                param children: list of token tree leaf node children
                """
                groups = []
                in_field = False

                # Build the field access groups.
                for child in children:
                    if in_field:
                        groups[-1].append(child)
                    else:
                        groups.append([child])

                    if child.data and child.data[2] == ".":
                        in_field = True
                    else:
                        in_field = False

                # Create filler nodes above each dotted name group.
                result = []
                for group in groups:
                    if len(group) == 1:
                        result.extend(group)
                    else:
                        result.append(create_filler_node(children=group))
                return result

            def rightmost_leaf(token_tree: TokenTreeType) -> TokenTreeType:
                """
                Return the rightmost leaf node in @tree

                :param token_tree: token tree node
                """
                if token_tree.children:
                    return rightmost_leaf(token_tree.children[-1])
                else:
                    return token_tree

            def build_token_tree_helper(
                node: AST,
                tokens: List[TokenTuple],
                ast_ranges: Dict[int, Tuple[int, int]],
                parent: Optional[TokenTreeType] = None,
                prefix: str = "",
            ) -> TokenTreeType:
                """
                Recursive helper function creating a tree of lexer tokens
                from @node and @tokens for use in Simplified AST creation.

                :param node: generic AST node
                :param tokens: list of tokens in @node's source range
                :param ast_ranges: dictionary mapping ASTs to start/stop offsets
                :param parent: token tree parent
                :param prefix: text to precede the next token tree node
                """
                tree = TokenTree(parent=parent)
                token_groups = build_token_groups(node, tokens, ast_ranges)

                for child_ast, child_tokens in token_groups:
                    if child_ast and token_tree_children(child_ast):
                        # Recursively create the new node to add to the tree.
                        new_child = build_token_tree_helper(
                            child_ast,
                            child_tokens,
                            ast_ranges,
                            parent=tree,
                            prefix=prefix,
                        )

                        # Clear the prefix, a new non-leaf node has been created.
                        prefix = ""

                        # Perform post-processing fixups on the new child.
                        new_child = create_keyword_statement_node(new_child, child_ast)
                        new_child = create_multiple_arguments_node(new_child, child_ast)
                        new_child = create_subscript_node(new_child, child_ast)
                        new_children = flatten_catch_asts(new_child, child_ast)

                        # Add the new child/children to the tree.
                        tree.children.extend(new_children)

                    else:
                        # Perform pre-processing fixups on the child tokens.
                        child_tokens = combine_string_tokens(child_ast, child_tokens)
                        child_tokens = callsite_tokens(child_ast, child_tokens)

                        # Create the new children leaf nodes to add to the tree.
                        new_children = []
                        for token in child_tokens:
                            _, token_type, text = token

                            if Token.Text in token_type.split():
                                prefix += text
                            else:
                                new_child = TokenTree(
                                    data=token,
                                    parent=tree,
                                    prefix=prefix,
                                )
                                new_children.append(new_child)
                                prefix = ""

                        # Perform post-processing fixups on the child leafs.
                        new_children = group_fields(new_children)

                        # Add the children to the tree.
                        tree.children.extend(new_children)

                # Add remaining non-AST text to the rightmost leaf's suffix.
                rightmost_leaf(tree).suffix = prefix

                return tree

            text = function.source_text
            lexer = get_lexer(function.language)
            tokens = list(lexer.get_tokens_unprocessed(text))
            line_lengths = [len(line) for line in text.splitlines()]
            ast_ranges = build_ast_ranges(function, line_lengths)
            body = function.function_body()

            return build_token_tree_helper(body, tokens, ast_ranges)

        def is_symbol_token(token: TokenTuple) -> bool:
            """
            Return true if the given @token is a language operator,
            keyword, or punctuation.

            :param token: lexer token type
            """

            SYMBOL_TOKEN_TYPES = {Token.Operator, Token.Keyword, Token.Punctuation}
            _1, token_type, _2 = token
            return set(token_type.split()).intersection(SYMBOL_TOKEN_TYPES)

        def label(node: TokenTreeType) -> str:
            """
            Return the label for the simplified parse tree of node.

            The label of a simplified parse tree is obtained by concatenating
            all the elements of the list representing the tree as follows:
              - If an element is a keyword token, the value of the token is
                used for concatenation.
              - If an element is a non-keyword token or a simplified parse
                tree, the special symbol # is used for concatenation.

            :param node: generic tree node
            :return: label for @node
            """
            result = ""

            for child in node.children:
                if child.data and is_symbol_token(child.data):
                    result += child.data[2]
                else:
                    result += "#"

            return result

        def is_var_decl(ast: AST) -> bool:
            """
            Return TRUE if AST is a variable declaration.
            """
            return isinstance(ast, (IdentifierAST, VariableDeclarationAST, CatchAST))

        def simplified_ast_helper(
            node: TokenTreeType,
            parent: Optional[SimplifiedASTType] = None,
            path: ASTPath = (),
        ) -> SimplifiedASTType:
            """
            Create a simplified AST from the given @node.

            :param node: generic tree node
            :param parent: simplified AST parent
            :param path: path to node from the root of the simplified AST tree
            :return: simplified AST representation of @node
            """
            ast = SimplifiedAST(data=label(node), parent=parent, path=path)

            children = []
            for i, c in enumerate(node.children):
                if c.children:
                    children.append(
                        simplified_ast_helper(
                            c,
                            parent=ast,
                            path=path + (i,),
                        )
                    )
                elif c.data:
                    data = dict()

                    data["leading"] = c.prefix
                    data["trailing"] = c.suffix
                    data["token"] = normalize_quotes(c.data[2])
                    if not is_symbol_token(c.data):
                        data["leaf"] = True

                    children.append(
                        SimplifiedAST(
                            data=data,
                            parent=ast,
                            path=path + (i,),
                        )
                    )
            ast._children = children

            return ast

        # Create the simplified AST from the function.
        return simplified_ast_helper(build_token_tree(function))
