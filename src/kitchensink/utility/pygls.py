"""
Collection of utilities related to the pygls library.
"""

from pygls.protocol import LanguageServerProtocol


class NonTerminatingLanguageServerProtocol(LanguageServerProtocol):
    """
    language server protocol implementation which ignores
    exit/shutdown/connection_lost messages from the client
    """

    def bf_exit(self, *args):
        pass

    def bf_shutdown(self, *args):
        pass

    def connection_lost(self, *args):
        pass
