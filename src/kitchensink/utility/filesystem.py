"""
Utility functions for corpus management related to the filesystem.
"""

from cchardet import detect
from mimetypes import guess_type
from pathlib import Path
from typing import List, Optional, Tuple


def python_files(directory: Path) -> List[Path]:
    """
    Retrieve the python files in @directory, recursive.

    :param directory: directory to search
    :return: list of paths to python files in @directory
    """

    def is_python(path: Path) -> bool:
        """
        Predicate for python files.

        :param path: file path to evaluate
        :return: true if @path represents a python file.
        """
        if path.is_file():
            return guess_language(path) == "python"

    return [f.resolve() for f in directory.rglob("*.*") if is_python(f)]


def cxx_files(directory: Path) -> List[Path]:
    """
    Return the C/C++ files in @directory, recursive.

    :param directory: directory to search
    :return: list of paths to c/c++ files in @directory
    """

    def is_cxx(path: Path) -> bool:
        """
        Predicate for C/C++ files.

        :param path: file path to evaluate
        :return: true if @path represents a c/c++ file.
        """
        if path.is_file():
            return guess_language(path) in ["c", "cpp"]

    return [f.resolve() for f in directory.rglob("*.*") if is_cxx(f)]


def slurp(path: Path) -> str:
    """
    Slurp the file at @path and return the text.

    :param path: file to read
    :return: text of the file
    """
    with open(path, "rb") as f:
        b = f.read()
        encoding = detect(b)["encoding"] or "utf-8"
        return b.decode(encoding)


def safe_slurp(path: Path) -> str:
    """
    Slurp the file at @path and return the text.  If an error occurs,
    return ''.

    :param path: file to read
    :return: text of the file or '' on error
    """
    try:
        return slurp(path)
    except ValueError:
        return ""
    except LookupError:
        return ""


def is_empty_file(path: Path) -> bool:
    """
    Predicate for testing if the file at @path is empty.

    :param path: path to the file to test
    :return: true if the file at @path is empty
    """
    return safe_slurp(path).strip() == ""


def guess_encoding(path: Path) -> Tuple[str, float]:
    """
    Return a pair (encoding, confidence) representing our best
    guess at the character encoding from the file at @path
    and our confidence between [0.0, 1.0] of that guess.

    :param path: path to guess encoding for
    :return: (encoding, confidence) pair
    """
    with open(path, "rb") as f:
        b = f.read()
        encoding = detect(b)
        return encoding["encoding"] or "UTF-8", encoding["confidence"] or 0.0


def guess_language(path: Path) -> Optional[str]:
    """
    Guess the source code language of the file at @path.

    :param path: file to guess language for
    :return: string representing the source code language at @path, if possible
    """
    tp = guess_type(str(path))[0]

    if tp == "text/x-chdr" or tp == "text/x-csrc":
        return "c"
    elif tp == "text/x-c++hdr" or tp == "text/x-c++src":
        return "cpp"
    elif tp == "text/x-python":
        return "python"
    elif tp == "application/javascript":
        return "javascript"
    elif tp == "text/x-java":
        return "java"
