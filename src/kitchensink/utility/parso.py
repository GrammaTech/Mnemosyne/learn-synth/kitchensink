"""
Collection of utility functions related to parso ASTs.

The python grammar from which the parso ASTs are derived
may be found at https://docs.python.org/3.8/reference/grammar.html
"""

from collections import OrderedDict, deque
from functools import lru_cache
from hashlib import md5
from re import match
from sys import byteorder
from typing import Callable, Dict, List, Optional, Tuple, Union

from jedi import Script
from jedi.api.classes import Signature
from parso.python.tree import (
    Class,
    EndMarker,
    ExprStmt,
    Flow,
    ForStmt,
    Function,
    KeywordStatement,
    Lambda,
    ImportName,
    ImportFrom,
    Name,
    Number,
    Newline,
    Operator,
    Scope,
    String,
    WithStmt,
    BaseNode,
    PythonBaseNode,
    PythonNode,
)

from kitchensink.utility.iterators import reversed_enumerate


def is_function(node: BaseNode) -> bool:
    """
    Predicate for function definitions.

    :param node: generic AST node
    :return: true if @node is a function definition
    """
    return type(node) is Function


def is_function_callsite(node: BaseNode) -> bool:
    """
    Predicate for function callsites (e.g. "foo(a)").

    :param node: generic AST node
    :return: true if @node is a function callsite
    """
    result = True

    try:
        # This is a function callsite if (1) the node is an 'atom_expr'
        # from the python grammar and (2) the last child is an argument
        # list.
        result &= node.type == "atom_expr"  # (1)
        result &= is_arglist(node.children[-1])  # (2)
    except AttributeError:
        result = False

    return result


def is_incomplete_function_callsite(node: BaseNode) -> bool:
    """
    Predicate for incomplete function callsites (e.g "foo(a,").

    :param node: generic AST node
    :return: true if @node is an incomplete function callsite
    """
    result = True

    try:
        # Find the index of the opening parenthesis in the function callsite.
        index = rchild_index(is_opening_paren, node) or -1

        # If the AST prior to the parenthesis contains a name, this is an
        # incomplete function callsite.
        result &= index > 0 and names(node.children[index - 1]) != []
    except AttributeError:
        result = False

    return result


def is_object_method(callsite: BaseNode) -> bool:
    """
    Return true if callsite is an object method call (e.g. `obj.foo()`).
    Note this method will return false for constructors.

    :param callsite: function or method callsite
    :return: true if callsite is an object method call
    """
    # If the name of the first child of the callsite is in scope
    # (e.g. obj.foo(...)) or the first child is not a name (e.g.
    # ",".join(...)), this is likely a method call.  This may give
    # false positives for variables which point to functions
    # (e.g. f = functools.partial(...)), but we are more concerned
    # about false negatives in this application.

    # TODO: Rename is_function_callsite(...) as it currently handles
    # both methods and free functions, which may be confusing.
    assert is_function_callsite(callsite)

    obj = callsite.children[0]
    trailer = callsite.children[1:-1]

    if trailer:
        if any(not is_dotted_name_trailer(c) for c in trailer):
            return True
        if isinstance(obj, Name) and obj.value in in_scope_names(callsite):
            return True
    return False


def is_opening_paren(node: BaseNode) -> bool:
    """
    Predicate for opening parenthesis.

    :param node: generic AST node
    :return: true if @node is an opening parenthesis
    """
    try:
        return node.value == "("
    except AttributeError:
        return False


def starts_with_opening_paren(node: BaseNode) -> bool:
    """
    Predicate for ASTs beginning with an opening paranthesis.

    :param node: generic AST node
    :return: true if @node or @node's first leaf is an opening parenthesis
    """
    return is_opening_paren(node.get_first_leaf())


def is_arglist(node: BaseNode) -> bool:
    """
    Predicate for argument list ASTs.

    :param node: generic AST node
    :return: true if @node is an argument list
    """
    result = True

    try:
        result &= node.type == "trailer"
        result &= node.get_first_leaf().value == "("
        result &= node.get_last_leaf().value == ")"
    except AttributeError:
        result = False

    return result


def is_empty_arglist(node: BaseNode) -> bool:
    """
    Predicate for empty argument lists.

    :param node: generic AST node
    :return: true if @node is an empty argument list
    """
    return is_arglist(node) and len(node.children) == 2


def is_constant(node: BaseNode) -> bool:
    """
    Predicate for constants (e.g. strings, numbers, booleans).

    :param node: generic AST node
    :return: true if @node is a constant (e.g. string, number, boolean)
    """

    def is_keyword_constant(node: BaseNode) -> bool:
        """
        Predicate for keyword constants such as 'True', 'False', or 'None'.

        :param node: generic AST node
        :return: true if @node is a keyword constant
        """
        try:
            return node.value in ["True", "False", "None"]
        except AttributeError:
            return False

    def is_empty_collection(node: BaseNode) -> bool:
        """
        Predicate for empty collections such as dict() or {}.

        :param node: generic AST node
        :return: true if @node represents the instantiation of an empty collection
        """
        result = True

        try:
            if is_function_callsite(node):
                # Handle explicit creation of empty collections
                # (e.g. 'list()', 'dict()').  Test if the function
                # name is a builtin and the argument list is empty.
                builtins = {
                    "bytearray",
                    "bytes",
                    "complex",
                    "dict",
                    "float",
                    "frozenset",
                    "int",
                    "list",
                    "set",
                    "str",
                    "tuple",
                }

                # The first child is the function name and the second
                # child is the argument list.
                function_name = node.children[0].value
                arglist = node.children[1]

                result &= function_name in builtins
                result &= is_empty_arglist(arglist)
            else:
                # Handle empty list, dict (e.g. '[]', '{}').
                result &= node.type == "atom"
                result &= all(lambda c: isinstance(c, Operator) for c in node.children)
        except AttributeError:
            result = False

        return False

    return (
        isinstance(node, Number)
        or isinstance(node, String)
        or is_keyword_constant(node)
        or is_empty_collection(node)
    )


def is_dotted_name_trailer(node: BaseNode) -> bool:
    """
    Predicate for dotted name trailers in the form '.' NAME from the
    python grammar.

    :param node: generic AST node
    :return: true if @node is a dotted name trailer
    """
    result = True

    try:
        # This is a dotted name trailer if (1) the node is a 'trailer'
        # from the python grammar, (2) first leaf is an '.' operator,
        # and (3) the last leaf is a name.
        result &= node.type == "trailer"  # (1)
        result &= node.get_first_leaf().value == "."  # (2)
        result &= node.get_last_leaf().type == "name"  # (3)
    except AttributeError:
        result = False

    return result


def is_keyword_argument(node: BaseNode) -> bool:
    """
    Predicate for keyword arguments in the form key=value.

    :param node: generic AST node
    :return: true if @node is a keyword argument
    """
    result = True

    try:
        result &= isinstance(node, PythonNode)
        result &= node.type == "argument"
        result &= len(node.children) > 2
        result &= node.children[0].type == "name"
        result &= node.children[1].value == "="
    except AttributeError:
        result = False

    return result


def is_incomplete_keyword_argument(node: BaseNode) -> bool:
    """
    Predicate for incomplete keyword arguments in the form key=.

    :param node: generic AST node
    :return: true if @node is an incomplete keyword argument
    """
    result = True

    try:
        parent = node.parent
        last_leaf = parent.get_last_leaf()

        # This is an incomplete keyword argument if (1) the parent
        # is an incomplete function callsite, (2) the parent
        # ends in an '=' operator, and (3) the leaf prior to
        # the '=' operator is a name.
        result &= is_incomplete_function_callsite(parent)  # (1)
        result &= last_leaf.value == "="  # (2)
        result &= last_leaf.get_previous_sibling().type == "name"  # (3)
    except AttributeError:
        result = False

    return result


def is_field_access(node: BaseNode) -> bool:
    """
    Predicate for field accesses in the form obj.value

    :param node: generic AST node
    :return: true if @node is a field access
    """
    result = True

    try:
        children = node.children

        # The field value is the second child if one exists.
        value = children[1] if len(children) > 1 else None

        # This is a field access if (1) the node is an 'atom_expr'
        # from the python grammar, (2) the node is not a function
        # callsite, and (3) the value is a dotted name in the
        # form "'.' NAME".
        result &= node.type == "atom_expr"  # (1)
        result &= not is_function_callsite(node)  # (2)
        result &= is_dotted_name_trailer(value)  # (3)
    except AttributeError:
        result = False

    return result


def get_keyword(node: BaseNode) -> Optional[BaseNode]:
    """
    If @node is a keyword argument, retrieve the key for the
    key=value form.

    :param node: generic AST node
    :return: keyword AST for the keyword argument
    """
    if is_keyword_argument(node):
        # The keyword is the first child if we have
        # the complete form "key=value".
        return node.children[0]
    elif is_incomplete_keyword_argument(node):
        # The keyword is the second to last child
        # of the parent if we have the incomplete
        # form "key=".
        return node.parent.children[-2]
    else:
        return None


def get_keyword_value(node: BaseNode) -> Optional[BaseNode]:
    """
    If @node is a keyword argument, retrieve the value for the
    key=value form.

    :param node: generic AST node
    :return: value of the keyword argument
    """
    if is_keyword_argument(node):
        # The keyword value is the last child if we
        # have the form key=value.
        return node.children[-1]
    else:
        return None


def get_field(node: BaseNode) -> Optional[BaseNode]:
    """
    If @node is a field access of the form obj.value,
    retrieve VALUE.

    :param node: generic AST node
    :return: value of the field access
    """
    if is_field_access(node):
        return node.children[1].children[1]
    else:
        return None


@lru_cache(maxsize=512)
def callsite_signature(script: Script, callsite: PythonNode) -> Optional[Signature]:
    """
    Retrieve the signature of the function at @callsite in @script
    if possible.  In the case of multiple matching signatures in
    the same module, the first matching signature is returned
    as this is sufficient for all clients of this function.

    :param script: python script representation containing @callsite
    :param callsite: function callsite node
    :return: signature of the function at callsite, if possible
    """
    assert is_function_callsite(callsite) or is_incomplete_function_callsite(callsite)

    # Get the position of the last non-closing parenthesis
    # in the function call.
    line, col = callsite.get_last_leaf().end_pos
    if is_function_callsite(callsite):
        # Adjustment for closing parenthesis in complete
        # function calls.
        col = col - 1

    # Workaround for a bug in Jedi.  Each time `get_signatures`
    # is called, the inferred_element_counts dictionary is
    # updated with new values; in a bid to limit recursion,
    # signature inference will stop when inference counts become
    # too large.  Unfortunately, this dictionary of counts is not
    # reset with each call to `get_signatures` so inference can
    # be short-circuited in an attempt to limit recursion if
    # multiple independent calls are made to `get_signatures`.
    # We reset this dictionary manually here.
    #
    # See _limit_value_infers in jedi/syntax_tree.py.
    script._inference_state.inferred_element_counts = {}

    try:
        # Get the function signatures for the given position.
        signatures = script.get_signatures(line, col)
    except Exception:
        # Catching exception here as `get_signatures` may throw
        # ValueError, KeyError, and RecursionError among other,
        # perhaps more rare, exceptions.
        signatures = []

    # Return the first signature if they all are in the
    # same module or only one signature is returned.
    module_names = set(s.module_name for s in signatures)
    if len(module_names) == 1:
        signature = signatures[0]
    else:
        signature = None

    return signature


@lru_cache(maxsize=512)
def callsite_function(script: Script, callsite: BaseNode) -> Optional[str]:
    """
    Retrieve the function name at the @callsite in @script.

    :param script: python script representation containing @callsite
    :param callsite: function callsite node
    :return: name of the function at callsite, if possible
    """
    assert is_function_callsite(callsite) or is_incomplete_function_callsite(callsite)

    signature = callsite_signature(script, callsite)
    if signature:
        # In the simplest case, we are to retrieve the signature
        # of the function at the callsite and return the name directly.
        return signature.name
    else:
        # If the signature cannot be inferred, retrieve the function
        # name directly from the callsite.  First, find the index of
        # the opening parenthesis in the function callsite.
        index = rchild_index(starts_with_opening_paren, callsite)

        # Retrieve the child prior to the parenthesis.
        child = callsite.children[index - 1]

        # Derive and return the function name from the AST prior
        # to the opening parenthesis.
        if child.type == "name":
            # foo(args)
            return child.value
        elif is_dotted_name_trailer(child):
            # module.foo(args)
            return child.get_last_leaf().value
        else:
            # A more complex case, where the function name cannot
            # be statically determined.
            return None


@lru_cache(maxsize=512)
def callsite_module(
    script: Script,
    callsite: BaseNode,
) -> Optional[str]:
    """
    Retrieve the module name of the function at @callsite in @script.

    :param script: python script representation containing @callsite
    :param callsite: function callsite node
    :return: module of the function at callsite, if possible
    """
    assert is_function_callsite(callsite) or is_incomplete_function_callsite(callsite)

    signature = callsite_signature(script, callsite)
    if signature and signature.module_path:
        # In the simplest case, we are to retrieve the signature
        # of the function at the callsite and return the module directly.
        return signature.module_name
    else:
        # If the signature cannot be inferred, retrieve the module
        # name directly from the callsite.

        def module_start_index(callsite: BaseNode, function_name: str) -> int:
            """
            Retrieve the starting index in @callsite's children of the module name.

            :param callsite: function callsite
            :param function_name: name of the function at @callsite
            :return: starting index in @callsite's children of the module name
            """
            result = 0
            end = module_end_index(callsite, function_name)

            # Find the index of the first name AST in callsite's children.
            for i, c in reversed_enumerate(callsite.children[:end]):
                if c.type == "name":
                    # Found a name, the beginning of the module prefix.
                    # Set result to i and break.
                    result = i
                    break
                elif not is_dotted_name_trailer(c):
                    # Found something other than a ".name", set
                    # result to the module end index and break.
                    result = end
                    break

            return result

        def module_end_index(callsite: BaseNode, function_name: str) -> int:
            """
            Retrieve the ending index in @callsite's children of the module name.

            :param callsite: function callsite
            :param function_name: name of the function at @callsite
            :return: ending index in @callsite's children of the module name
            """

            def is_dotted_function_name(node: BaseNode) -> bool:
                """
                Predicate for nodes maching @function_name with a leading dot.

                :param node: generic AST node
                :return: true if @node's source code matches @function_name
                    with a leading dot.
                """
                return (
                    is_dotted_name_trailer(node)
                    and node.get_last_leaf().value == function_name
                )

            return rchild_index(is_dotted_function_name, callsite) or 0

        def prefixes(prefix_asts: List[BaseNode]) -> List[List[BaseNode]]:
            """
            Generate incrementally larger sublists of @prefix_asts.

            :prefix_ast: list of module prefix ASTs
            :return: list of incrementally larger sublist of @prefix_asts
            """
            for i in range(1, len(prefix_asts) + 1):
                yield prefix_asts[:i]

        def code(asts: List[BaseNode]):
            """
            Get the code for the given list of ASTs without leading
            or trailing whitespace.

            :param asts: list of ASTs to join as source code
            :return: source code for @asts without leading or
                trailing whitespace
            """
            return "".join(a.get_code(False) for a in asts)

        def no_module_name_present(node: BaseNode, function_name: str) -> bool:
            """
            Predicate for callsite nodes without module names.

            :param node: generic AST node
            :param function: name of the function at @node
            :return: true if no module name is present at the callsite and the
                function is the first child
            """
            return function_name == node.get_first_leaf().value

        # Retrieve a mapping of aliases -> modules and names -> modules
        # from the import and import from statements in scope at callsite.
        # These mappings will be utilized when determining the module
        # at the callsite.
        import_aliases = _import_aliases(callsite)
        import_names = _import_names(callsite)

        # Retrieve the child ASTs composing the module prefix.
        function_name = callsite_function(script, callsite)
        start = module_start_index(callsite, function_name)
        end = module_end_index(callsite, function_name)
        module_prefix_asts = callsite.children[start:end]

        if module_prefix_asts:
            # Iterate over the ASTs specifying the module and attempt to find
            # any sublist in IMPORT_ALIASES to properly handle
            # "import module as alias" statements.  After accounting for aliases,
            # append any remaining submodule parts to the module name.
            for prefix in prefixes(module_prefix_asts):
                prefix_code = code(prefix)
                if prefix_code in import_aliases:
                    postfix_code = code(module_prefix_asts[len(prefix) :])
                    return import_aliases[prefix_code] + postfix_code
        elif no_module_name_present(callsite, function_name):
            # Special case for when the module isn't specified.  Check
            # if the function name is in a "from module import name"
            # statement and if so, return the module in the from statement.
            return import_names.get(function_name)

        # If we reach this statement, we have found a case where the module
        # name cannot be ascertained or the function is defined in the
        # current file.
        return None


def _import_aliases(node: BaseNode) -> Dict[str, str]:
    """
    Build a mapping of alias -> import name from the import statements
    in scope at @node for use in `callsite_module`.  If there is no alias,
    the mapping will still exist as an identity from import name ->
    import name.

    :param node: generic AST node
    :return: mapping of alias -> import name for the import statements in
        scope at @node
    """

    def is_as_keyword(node: BaseNode) -> bool:
        """
        Predicate for nodes which are AS keywords.

        :param node: generic AST node
        :return: true if @node is an AS keyword.
        """
        try:
            return node.type == "keyword" and node.value == "as"
        except AttributeError:
            return False

    def contains_as_keyword(node: BaseNode) -> bool:
        """
        Predicate for nodes containing AS keywords.

        :param node: generic AST node
        :return: true if @node contains an AS keyword.
        """
        return filter_asts(is_as_keyword, node) != []

    result = dict()

    for import_ast in in_scope_imports(node):
        # Retrieve the module name from the AST.
        module = ".".join([a.value for a in import_ast.get_paths()[0]])

        # Get the alias if an 'as' keyword is present in the import
        # statement.  Otherwise, use the module name as an identity.
        if contains_as_keyword(import_ast):
            alias = import_ast.get_defined_names()[0].value
        else:
            alias = module

        # Populate the alias -> module mapping in the result dict.
        result[alias] = module

    return result


def _import_names(node: BaseNode) -> Dict[str, str]:
    """
    Build a mapping of name -> import out of the `from ... import`
    statements in scope at @node for use in `callsite_module`.

    :param node: generic AST node
    :return: mapping of name -> import for the import from statements in
        scope at @node
    """
    result = dict()

    for import_from_ast in in_scope_import_froms(node):
        # Retrieve the module name from the AST.
        module = ".".join([a.value for a in import_from_ast.get_from_names()])

        # Retrieve the names being imported from the AST.
        names = [a.value for a in import_from_ast.get_defined_names()]

        # Update the result mapping.
        for name in names:
            result[name] = module

    return result


def callargs(callsite: BaseNode) -> List[PythonBaseNode]:
    """
    Return the callargs for the function @callsite.

    :param callsite: function callsite node
    :return: list of callargs to the function at @callsite
    """
    assert is_function_callsite(callsite) or is_incomplete_function_callsite(callsite)

    def collect_callargs(ast: BaseNode) -> List[BaseNode]:
        """
        Return the callargs in @ast or its children if @ast is an
        argument list.

        :param ast: AST node at a function callsite
        :return: callargs if @ast is a callarg itself or argument list
        """
        if ast.type == "arglist":
            # For ASTs which are argument lists themselves,
            # (e.g. a,b,c) collect the arguments.
            return [callarg for callarg in ast.children if callarg.type != "operator"]
        elif ast.type != "operator":
            return [ast]
        else:
            return []

    last = callsite.children[-1]

    if is_function_callsite(callsite):
        # This is a complete function callsite (e.g. foo(), foo(a),
        # foo(a,b,c).  The potential callargs in the trailer's children.
        potential_callargs = last.children
    else:
        # This is an incomplete function callsite, find the index of
        # the opening parenthesis.  The potential callargs come after
        # the opening parenthesis.
        index = rchild_index(is_opening_paren, callsite) + 1
        potential_callargs = callsite.children[index:]

    # Iterate over the potential callargs at the callsite and
    # collect the callargs.
    callargs = []
    for child in potential_callargs:
        callargs.extend(collect_callargs(child))

    return callargs


def normalize_callarg(callarg: PythonBaseNode) -> PythonBaseNode:
    """
    Transform a callarg into a normalized form where field
    accesses are resolved to the innermost field and keyword
    arguments are resolved to the keyword value.

    :param callarg: function callarg node
    :return: callarg normalized where keyword arguments and object
        names are elided
    """
    # If CALLARG is a keyword argument in the form key=value,
    # retrieve value.
    if is_keyword_argument(callarg):
        callarg = get_keyword_value(callarg)

    # While CALLARG is a field access of the form parent.value,
    # retrieve value.
    while is_field_access(callarg):
        callarg = get_field(callarg)

    return callarg


def normalize_quotes(constant: str) -> str:
    """
    If possible, replace any leading/trailing single quotes with double
    quotes in @constant.

    :param constant: string value
    :return: @constant with leading/trailing single quotes replaced with
        double quotes if possible
    """
    if match("^'[^'\"]+'$", constant):
        # Replace leading and trailing single quotes with double quotes.
        result = '"' + constant[1:-1] + '"'
    else:
        # Passthru constant directly as a result.
        result = constant

    return result


def last_argument_position(callsite: BaseNode) -> int:
    """
    Return the position of the last argument in the incomplete
    function @callsite.

    :param callsite: incomplete function callsite
    :return: integer position of the argument we are predicting
    """
    assert is_incomplete_function_callsite(callsite)

    try:
        arglist = callsite.children[-1].children
    except AttributeError:
        arglist = []

    commas = [c for c in arglist if c.type == "operator" and c.value == ","]
    return len(commas)


def hash_function(node: Function) -> int:
    """
    Return a deterministic hashcode for the given function.

    :param node: function AST
    :return: 64-bit deterministic hashcode for @node
    """
    code = node.get_code(False)
    digest = md5(code.encode("utf-8")).digest()
    return int.from_bytes(digest[:8], byteorder=byteorder, signed=True)


def function_body(function: Function) -> BaseNode:
    """
    Return the body of the given @function.

    :param function: function AST
    :return: body of @function
    """
    assert is_function(function)

    children = [
        child
        for child in function.children
        if not isinstance(child, EndMarker) and not isinstance(child, Newline)
    ]
    body = children[-1]

    return body


def names(node: BaseNode) -> List[Name]:
    """
    Retrieve the names in @node and its children.

    :param node: generic AST node
    :return: names in @node and its children
    """
    return filter_asts(lambda a: isinstance(a, Name), node)


def functions(node: BaseNode) -> List[Function]:
    """
    Retrieve the functions in @node and its children.

    :param node: generic AST node
    :return: functions in @node and its children
    """
    return filter_asts(is_function, node)


def function_callsites(node: BaseNode) -> List[PythonBaseNode]:
    """
    Return the function callsites in @node and its children.

    :param node: generic AST node
    :return: function callsites in @node and its children
    """
    return filter_asts(is_function_callsite, node)


def ancestors(node: BaseNode) -> List[BaseNode]:
    """
    Retrieve @node and its ancestors (parents).

    :param node: generic AST node
    :return: node and its ancestors (parents)
    """
    results = []
    while node:
        results.append(node)
        node = node.parent
    return results


def recursive_children(node: BaseNode) -> List[BaseNode]:
    """
    Retrieve @node and its children recursively in pre-order traversal.

    :param node: generic AST node
    :return: node and its children in pre-order traversal
    """
    results = [node]

    try:
        children = node.children
        for child in children:
            child_results = recursive_children(child)
            results.extend(child_results)
        return results
    except AttributeError:
        return results


def filter_asts(
    predicate: Callable[[BaseNode], bool], node: BaseNode
) -> List[BaseNode]:
    """
    Retrieve a list of ASTs in @node and its children which satisfy
    @predicate.

    :param predicate: boolean predicate taking an AST node parameter
    :param node: generic AST node
    :return: filtered list of ASTs in @node and its children satisfying
        @predicate
    """
    return list(filter(predicate, recursive_children(node)))


def filter_ancestors(
    predicate: Callable[[BaseNode], bool], node: BaseNode
) -> List[BaseNode]:
    """
    Retrieve a list of ASTs in @node and its ancestors which
    satisfy @predicate.

    :param predicate: boolean predicate taking an AST node parameter
    :param node: generic AST node
    :return: filtered list of ASTs in @node and its ancestors satisfying
    """
    return list(filter(predicate, ancestors(node)))


def ancestor_satisfying_predicate(
    predicate: Callable[[BaseNode], bool], node: BaseNode
) -> Optional[BaseNode]:
    """
    Retrieve the first ancestor of @node (including @node itself)
    satisfying @predicate, if possible.

    :param predicate: boolean predicate taking an AST node parameter
    :param node: generic AST node
    :return: the first ancestor of @node (including @node itself)
        satisfying @predicate, if possible.
    """
    nodes = filter_ancestors(predicate, node)
    return nodes[0] if nodes else None


def child_index(predicate: Callable[[BaseNode], bool], node: BaseNode) -> Optional[int]:
    """
    Get the index of the first child in @node satisfying @predicate,
    if one exists.

    :param predicate: boolean predicate taking an AST node parameter
    :param node: generic AST node
    :return: index of the first child in @node satisfying @predicate,
        if one exists
    """
    return _child_index_helper(predicate, node, enumerate)


def rchild_index(
    predicate: Callable[[BaseNode], bool], node: BaseNode
) -> Optional[int]:
    """
    Get the index of the last child in @node satisfying @predicate,
    if one exists.

    :param predicate: boolean predicate taking an AST node parameter
    :return: index of the last child in @node satisfying @predicate,
        if one exists
    """
    return _child_index_helper(predicate, node, reversed_enumerate)


def _child_index_helper(
    predicate: Callable[[BaseNode], bool],
    node: BaseNode,
    enumerator: Callable[[List[BaseNode]], List[Tuple[int, BaseNode]]],
) -> Optional[int]:
    """
    Iterate over the children of @node in the order defined by @enumerator,
    returning the index of the first child in @node satisfying @predicate.

    :param predicate: boolean predicate taking an AST node parameter
    :param node: generic AST
    :param enumerator: function enumerating an iterable as (index, item) pairs
    :return: index of the first child of @node given by the @enumerator
        satisfying @predicate
    """
    for i, child in enumerator(node.children):
        if predicate(child):
            return i
    return None


def in_scope_imports(node: BaseNode) -> List[ImportName]:
    """
    Retrieve the ImportName ASTs in scope at @node.

    :param node: generic AST node
    :return: list of ImportName ASTs in scope
    """
    return [
        import_name_ast
        for import_name_ast in _in_scope_import_helper(node)
        if isinstance(import_name_ast, ImportName)
    ]


def in_scope_import_froms(node: Optional[BaseNode]) -> List[ImportFrom]:
    """
    Retrieve the ImportFrom ASTs in scope at @node.

    :param node: generic AST node
    :return: list of ImportFrom ASTs in scope
    """
    return [
        import_from_ast
        for import_from_ast in _in_scope_import_helper(node)
        if isinstance(import_from_ast, ImportFrom)
    ]


def _in_scope_import_helper(node: BaseNode) -> List[PythonBaseNode]:
    """
    Helper function returning both the ImportName and ImportFrom
    ASTs in scope at @node.

    :param node: generic AST node
    :return: list of ImportName and ImportFrom ASTs in scope
    """
    results = []

    while node:
        if isinstance(node, Scope):
            results.extend(list(node.iter_imports()))
        node = node.parent

    return results


def in_scope_names(node: BaseNode, global_names: bool = True) -> List[str]:
    """
    Return the names in scope at the given @node.

    :param node: generic AST node
    :param global_names: include names in scope globally
    :return: names in scope at @node
    """

    def is_suite(node: BaseNode) -> bool:
        """
        Predicate for suite ASTs.

        :param node: generic AST node
        :return: true if @node is a suite AST
        """
        return isinstance(node, PythonNode) and node.type == "suite"

    def is_simple_stmt(node: BaseNode) -> bool:
        """
        Predicate for simple_stmt ASTs.

        :param node: generic AST node
        :return: true if @node is a simple_stmt AST
        """
        return isinstance(node, PythonNode) and node.type == "simple_stmt"

    def is_nonlocal(node: BaseNode) -> bool:
        """
        Predicate for nonlocal statement ASTs.

        :param node: generic AST node
        :return: true if @node is a nonlocal statement AST
        """
        return isinstance(node, KeywordStatement) and node.keyword == "nonlocal"

    def is_global(node: BaseNode) -> bool:
        """
        Predicate for global statement ASTs.

        :param node: generic AST node
        :return: true if @node is global statement AST
        """
        return isinstance(node, KeywordStatement) and node.keyword == "global"

    def is_expr_stmt(node: BaseNode) -> bool:
        """
        Predicate for ExprStmt ASTs.

        :param node: generic AST node
        :return: true if @node is an ExprStmt AST
        """
        return isinstance(node, ExprStmt)

    def is_name(node: BaseNode) -> bool:
        """
        Predicate for Name ASTs.

        :param node: generic AST node
        :return: true if @node is a Name AST
        """
        return isinstance(node, Name)

    def is_destructuring_bind(node: BaseNode) -> bool:
        """
        Predicate for destructing bind ASTs.

        :param node: generic AST node
        :return: true if @node represents a destructuring bind AST
        """
        return isinstance(node, PythonNode) and node.type == "testlist_star_expr"

    def is_import_from(node: BaseNode) -> bool:
        """
        Predicate for import from ASTs.

        :param node: generic AST node
        :return: true if @node represents an import from AST
        """
        return isinstance(node, ImportFrom)

    def is_except_clause(node: BaseNode) -> bool:
        """
        Predicate for except clause ASTs.

        :param node: generic AST node
        :return: true if @node represents an except clause AST
        """
        return node.type == "except_clause"

    def is_named_expression(node: BaseNode) -> bool:
        """
        Predicate for named expression ASTs.

        :param node: generic AST node
        :return: true if @node represents a named expression AST
        """
        return node.type == "namedexpr_test"

    def is_clause_with_named_expressions(node: BaseNode) -> bool:
        """
        Predicate for clauses in control flow statements with named
        expression children.

        :param node: generic AST node
        :return: true if @node is a clause in a control flow statement
            with a named expression AST child.
        """
        result = True

        try:
            # This is a clause with a named expression if (1) the parent node
            # is a control flow statement, (2) the node itself is an 'atom'
            # from the python grammar with a test between parenthesis, and
            # (3) a named expression exists in a child of node.
            result &= isinstance(node.parent, Flow)  # (1)
            result &= node.type == "atom"  # (2)
            result &= node.get_first_leaf().value == "("  # (2)
            result &= node.get_last_leaf().value == ")"  # (2)
            result &= [is_named_expression(c) for c in node.children] != []  # (3)
        except AttributeError:
            result = False

        return result

    def is_if_stmt(node: BaseNode) -> bool:
        """
        Predicate for if statements.

        :param node: generic AST node
        :return: true if @node is an if statement
        """
        return node.type == "if_stmt"

    def is_loop_stmt(node: BaseNode) -> bool:
        """
        Predicate for loop statements.

        :param node: generic AST node
        :return: true if @node is a loop statement
        """
        return node.type == "for_stmt" or node.type == "while_stmt"

    def is_try_stmt(node: BaseNode) -> bool:
        """
        Predicate for try statements.

        :param node: generic AST node
        :return: true if @node is a try statement
        """
        return node.type == "try_stmt"

    def is_with_stmt(node: BaseNode) -> bool:
        """
        Predicate for with statements.

        :param node: generic AST node
        :return: true if @node is a with statement
        """
        return node.type == "with_stmt"

    def is_if_clause(node: BaseNode) -> bool:
        """
        Predicate for if statement clauses.

        :param node: generic AST node
        :return: true if @node is a test clause in an if statement
        """
        result = True

        try:
            parent = node.parent

            # This is a test clause in an if statement if (1) the
            # parent node is an if statement and (2) the node itself
            # is an 'atom' from the python grammar with a test between
            # parenthesis.
            result &= is_if_stmt(parent)  # (1)
            result &= node.type == "atom"  # (2)
            result &= node.get_first_leaf().value == "("  # (2)
            result &= node.get_last_leaf().value == ")"  # (2)
        except AttributeError:
            result = False

        return result

    def is_finally_keyword(node: BaseNode) -> bool:
        """
        Predicate for finally keywords.

        :param node: generic AST node
        :return: true if @node is a finally keyword
        """
        try:
            return node.value == "finally"
        except AttributeError:
            return False

    @lru_cache(maxsize=512)
    def prior_statements_in_scope(scope: BaseNode, node: BaseNode) -> List[BaseNode]:
        """
        Retrieve the statements in @scope prior to @node starting with
        the child previous to @node.

        :param scope: AST defining the scope containing node
        :param node: generic AST node in scope
        :return: statements in @scope prior to @node starting with the
            child previous to @node
        """
        children = scope.children
        stmts = deque()
        index = child_index(lambda c: c == node, scope) or len(children)

        for child in reversed(children[:index]):
            stmts.appendleft(child)
            if is_if_clause(child):
                break
            elif is_except_clause(child) or is_finally_keyword(child):
                try_block = scope.children[2] if scope.type == "try_stmt" else None
                if try_block:
                    stmts.extendleft(prior_statements_in_scope(try_block, node))
                break

        return list(stmts)

    @lru_cache(maxsize=512)
    def inner_names(node: BaseNode) -> List[str]:
        """
        Retrieve names affecting inner scopes.

        :param node: generic AST node
        :return: list of names affecting inner scopes
        """
        if isinstance(node, Function):
            # For functions, collect the names of all params.
            names = [param.name.value for param in node.get_params()]
            if not isinstance(node, Lambda):
                # For non-lambdas, include the function name as well.
                names.insert(0, node.name.value)
        elif isinstance(node, Class):
            # For classes, collect the name of the class.
            names = [node.name.value]
        elif isinstance(node, ForStmt) or isinstance(node, WithStmt):
            # For loops and with statements, collect the defined names.
            names = [name.value for name in node.get_defined_names()]
        else:
            names = []

        return names

    @lru_cache(maxsize=512)
    def outer_names(node: BaseNode) -> List[str]:
        """
        Retrieve names affecting outer scopes.

        :param node: generic AST node
        :return: list of names affecting outer scopes
        """
        names = []

        if is_simple_stmt(node):
            # The statement itself (w/out trailing whitespace) is the
            # first child.
            stmt = node.children[0]

            if is_nonlocal(stmt) or is_global(stmt):
                # For nonlocal and global statements, collect the defined names.
                names = filter(lambda a: isinstance(a, Name), stmt.children)
                names = [name.value for name in names]
            elif is_expr_stmt(stmt):
                # For assignment statements, collect the names defined on the
                # left hand side of the expression.  This may be a single
                # name (e.g. a = 0) or multiple names in the case of a
                # destructuring bind (e.g. [a, b] = [0, 1]).
                lhs = stmt.children[0]
                if is_name(lhs):
                    names = [lhs.value]
                else:
                    destructuring_bind = filter_asts(is_destructuring_bind, lhs)
                    names = destructuring_bind[0].children if destructuring_bind else []
                    names = filter(lambda a: isinstance(a, Name), names)
                    names = [name.value for name in names]
            elif is_import_from(stmt):
                # For import from statements, simply collect the names defined
                # after the import.
                names = stmt.get_defined_names()
                names = [name.value for name in names]
        elif is_except_clause(node):
            # For except clauses, collect the exception object name if present
            # (e.g. except Exception as obj).
            clause = node.children
            if len(clause) == 4:
                names = [clause[3].value]
        elif is_clause_with_named_expressions(node):
            # For clauses with named expression (e.g. (foo := bar()), collect
            # the name from the left side of the expression.
            named_expressions = filter_asts(is_named_expression, node)
            names = [
                expression.get_first_leaf().value for expression in named_expressions
            ]
        elif is_loop_stmt(node):
            suites = [child for child in reversed(node.children) if is_suite(child)]
            for suite in suites:
                for stmt in reversed(suite.children):
                    names.extend(outer_names(stmt))
            names.extend(inner_names(node))
        elif is_try_stmt(node) or is_with_stmt(node):
            body = [child for child in node.children if is_suite(child)]
            body_children = body[0].children if body else []
            for stmt in reversed(body_children):
                names.extend(outer_names(stmt))
        elif is_if_stmt(node):
            suites = [child for child in reversed(node.children) if is_suite(child)]
            suite_names = []
            for suite in suites:
                suite_names.append(set())
                for stmt in reversed(suite.children):
                    suite_names[-1].update(outer_names(stmt))
            names = list(set.intersection(*suite_names)) if suite_names else []

        return names

    def continue_up_scope_chain(node: BaseNode) -> bool:
        """
        Predicate for whether we should continue to collect names
        in the parent scope.

        :param node: generic AST node
        :return: TRUE if we should continue collecting in scope names
        """
        test_set = set()
        test_set.add(None)
        if not global_names:
            test_set.add(node.get_root_node())

        return node.parent not in test_set

    # Prepopulate names with special variables always in scope, if applicable.
    if global_names:
        names = [
            "__annotations__",
            "__builtins__",
            "__cached__",
            "__doc__",
            "__file__",
            "__loader__",
            "__name__",
            "__package__",
            "__spec__",
        ]
    else:
        names = []

    while continue_up_scope_chain(node):
        # Work up the node's parents, collecting names in
        # each scope.
        #
        # Note that we only collect names for stmts prior to NODE
        # in SCOPE's children but in python, some variables may be
        # in scope if stmts after NODE have been previously executed.
        # Unfortunately, there is no way of determining this with
        # perfect accuracy statically, so these names are not included,
        # likely with minimal impact.
        scope = node.parent
        stmts = prior_statements_in_scope(scope, node)

        # Update the list of names with the name declarations
        # for this scope and its child statements prior to NODE.
        names.extend(inner_names(scope))
        for stmt in stmts:
            names.extend(outer_names(stmt))

        node = scope

    return list(OrderedDict.fromkeys(names))


def ast_at_point(root: BaseNode, line: int, col: int) -> BaseNode:
    """
    Retrieve the node at @line and @col in @root.

    :param root: root node of an AST tree
    :param line: line with the AST (one-indexed)
    :param col: column with the AST (zero-indexed)
    :return: node at @line and @col in @root
    """
    position = (line, col)
    node = root.get_leaf_for_position(position, include_prefixes=True)

    # Adjust to get previous node if the cursor is on trailing whitespace.
    if node.start_pos > position or node.type == "endmarker":
        previous_node = node.get_previous_leaf()
        if previous_node is not None:
            node = previous_node

    return node


def identifier_at_point(
    root: BaseNode,
    line: int,
    col: int,
) -> Optional[Union[Name, Operator]]:
    """
    Retrieve the identifier nodes at @line and @col in @root, if possible.

    :param root: root node of an AST tree
    :param line: line with the AST (one-indexed)
    :param col: column with the AST (zero-indexed)
    :return: identifier nodes at @line and @col in @root
    """

    def is_name_or_dot(node: BaseNode) -> bool:
        """
        Predicate for name or "." ASTs.

        :param node: generic AST node
        :return: true if node is name or "."
        """
        result = isinstance(node, Name)
        result |= isinstance(node, Operator) and node.value == "."
        return result

    node = root.get_leaf_for_position((line, col), include_prefixes=True)
    identifier = []

    if is_name_or_dot(node):
        identifier.append(node)
    elif isinstance(node, Newline) and is_name_or_dot(node.get_next_leaf()):
        node = node.get_next_leaf()
        identifier.append(node)

    if identifier:
        previous_leaf = node.get_previous_leaf()
        while is_name_or_dot(previous_leaf):
            identifier.insert(0, previous_leaf)
            previous_leaf = previous_leaf.get_previous_leaf()
        next_leaf = node.get_next_leaf()
        while is_name_or_dot(next_leaf):
            identifier.append(next_leaf)
            next_leaf = next_leaf.get_next_leaf()

    return identifier if identifier else None
