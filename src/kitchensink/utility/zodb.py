"""
Collection of utility functions related to ZODB.
"""

import atexit
import multiprocessing
import pathlib

import ZODB
import ZODB.config


def create_database(dbpath: pathlib.Path) -> ZODB.DB:
    """
    Create a ZODB database from the directory at @dbpath.

    :param dbpath: path to the database to create or load
    :return: zodb database at path
    """
    conf = """
           %%import relstorage
           <zodb main>
             <relstorage>
               keep-history false
               <sqlite3>
                 data-dir %s
               </sqlite3>
             </relstorage>
           </zodb>
           """
    return ZODB.config.databaseFromString(conf % dbpath)


def close_database(database: ZODB.DB) -> None:
    """
    Close @database.  This function's anticipated use is as
    an exit handler in conjunction with the atexit module.

    :param database: zodb instance to close
    """
    database.close()


class DatabasePool:
    """
    pool of global database instances to be shared across objects
    in a long-running program
    """

    _pool = {}
    _lock = multiprocessing.Lock()

    @staticmethod
    def pool_create_database(dbpath: pathlib.Path) -> ZODB.DB:
        """
        Return a ZODB database from the directory at @dbpath.  The
        database is created once and cached after first use.

        :param dbpath: path to the database
        :return: zodb database at path
        """
        with DatabasePool._lock:
            if dbpath not in DatabasePool._pool:
                DatabasePool._pool[dbpath] = create_database(dbpath)
            return DatabasePool._pool[dbpath]

    @staticmethod
    def pool_close() -> None:
        """
        Close the database pool, removing all existing open databases.
        This method is called automatically upon program exit.
        """
        with DatabasePool._lock:
            for db in DatabasePool._pool.values():
                close_database(db)
            DatabasePool._pool.clear()


atexit.register(DatabasePool.pool_close)
