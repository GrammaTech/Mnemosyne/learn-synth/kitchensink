"""
Collection of utility functions related to iterators.
"""

from typing import Iterable, Iterator


def reversed_enumerate(collection: Iterable) -> Iterator:
    """
    Enumerate the items in @collection in reverse order.

    :param collection: bag of items satifying the iterable interface
    :return: a list of index, items pairs from collection in reverse order
    """
    return zip(reversed(range(len(collection))), reversed(collection))
