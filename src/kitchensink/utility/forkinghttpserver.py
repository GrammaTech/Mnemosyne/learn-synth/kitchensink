"""
Base class for Python muses that use JSON-RPC over HTTP (rather than
LSP per se). Forks new Python processes for each connection and
ensures that all logging (to stderr) is prefixed with the PID of the
process.
"""

from http.server import HTTPServer
import os
from socketserver import ForkingMixIn
import subprocess
import sys


class ForkingHTTPServer(ForkingMixIn, HTTPServer):
    """Forking HTTP server that prefixes stderr output with its pid."""

    def __init__(self, *args):
        self.proc = None
        super().__init__(*args)

    def finish_request(self, request, client_address):
        # Prefix all output to stderr with the child process PID.
        prefix = os.getpid()
        sed_script = "s/^/[" + str(prefix) + "] /"
        self.proc = subprocess.Popen(
            ["sed", sed_script], stdout=sys.stderr, stdin=subprocess.PIPE
        )
        stderrfd = sys.stderr.fileno()
        procfd = self.proc.stdin.fileno()
        os.dup2(procfd, stderrfd)
        return super().finish_request(request, client_address)

    def shutdown_request(self, request):
        super().shutdown_request(request)
        if self.proc:
            self.proc.kill()
