"""
Collection of utility functions related to strings.
"""

from typing import Text


def subtext(text: Text, line: int, col: int) -> Text:
    """
    Return all characters in TEXT prior to line and col.

    :param text: source text of the program
    :param line: end line of the substring (one-indexed)
    :param col: end column of the substring (zero-indexed)
    :return substring of text prior to position
    """
    lines = text.splitlines()

    result = lines[: line - 1]
    if 0 < line <= len(lines):
        result.append(lines[line - 1][:col])
    result = "\n".join(result)

    return result
