# Description

Collection of utilities to be shared across learn-synth repositories.

# Requirements

The kitchensink repository requires python 3.6 or higher.

# Installation

## Developer Install

If you are working on the development of this repository or
are a client looking for more frequent updates, you may wish
to install the package using the instructions given below.

The dependencies for this repository may be installed by executing
`pip3 install -r requirements.txt`.  If you are contributing you
will want to install the pre-commit hooks found in the
[pre-commit config](./pre-commit-config.yaml) by executing
`pre-commit install`.

After installing dependencies, you must place the repository's
src/ directory on your $PYTHONPATH.  From the base of this
repository, this may be accomplished with the following command:

```
export PYTHONPATH=$PWD/src:$PYTHONPATH
```

## Client Install

If you are using this library solely as a client, you may
create a python wheel file from this repository and install
it by executing the following sequence of commands:

```
python3 setup.py bdist_wheel --universal
pip3 install dist/*
```
